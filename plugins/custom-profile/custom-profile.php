<?php
/*
Plugin Name: custom profile form
Description: custom profile form
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/
function my_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?

	$interested_course_type = get_user_meta( $user->ID , 'interested_course_type', true);
	?><pre><?php print_r($interested_course_type); ?> </pre> <?php
    if ($interested_course_type) {
        //check for admins
		 return site_url() . "/candidate-ranks/";
    } else {
		return site_url() . "/candidate-ranks/";
    }
	return site_url() . "/candidate-ranks/";;
}
 
//add_filter( 'login_redirect', 'my_login_redirect', 1, 3 );
add_action('wp_enqueue_scripts', 'wp_profile_script_front_css');
add_action('wp_enqueue_scripts', 'wp_profile_script_front_js');
add_action('admin_enqueue_scripts', 'wp_profile_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {
	add_shortcode('edit_profile', 'wp_profile_form');
	add_shortcode('profile_header', 'wp_profile_header');
	add_shortcode('carrer_by_level_tex', 'carrer_by_level');
	add_shortcode('show_carrer_by_level', 'carrer_by_level_fnc');
	//add_shortcode('show_carrer_by_level', 'carrer_by_level_fnc');
}
function carrer_by_level_fnc($atts) {

			$args = array(
				'post_type' => 'career',
				'post_status' => 'publish',
				'posts_per_page' => -1
			);
			$postslist = get_posts( $args );
			echo '<div class="wpb_text_column "><div class="row">';
			if($postslist){
			
				foreach($postslist as $post){
					echo '<div class="col-md-6" href="'.get_permalink($post->ID).'"><h1>'.$post->post_title.'</h1></div><div class="col-md-6 career-btn">';
					echo do_shortcode('[show_gd_mylist_btn add_icon="fa fa-user-plus" remove_icon="fa fa-user-plus" add_label="Follow" remove_label="Un Follow" item_id="'.$post->ID.'"]');
					echo '</div><div class="col-md-12 career-level"><p>'.$post->post_content.'</p>';
					$post_meta = get_post_meta($post->ID);
					$basic_career_courses = $post_meta['basic_career_courses'];
					if($basic_career_courses){
						echo'<h6 class="course_list">Basic Courses List</h5>';
						foreach($basic_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					$intermediate_career_courses = $post_meta['intermediate_career_courses'];
					if($intermediate_career_courses){
						echo'<h6 class="course_list">Intermediate Courses List</h5>';
						foreach($intermediate_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					$advance_career_courses = $post_meta['advance_career_courses'];
					if($advance_career_courses){
						echo'<h6 class="course_list">Advance Courses List</h5>';
						foreach($advance_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					echo '</div>';
				}
			}else{
				echo 'No Carrer found';
			}
			echo '</div></div>';
}
function carrer_by_level($atts) {
	
	
	$taxonomies = get_terms( array(
			'taxonomy' => 'career_level',
			'hide_empty' => false
	) );
    foreach( $taxonomies as $category ) {
		
        if( $category->parent == 0 ) {
			echo '<div class="leve_row">';
			echo '<h2 class="career_level" label="'. esc_attr( $category->name ) . '">'.$category->slug.'</h2>';
			$args = array(
				'post_type' => 'career',
				'post_status' => 'publish',
				'tax_query' => array(
					array(
						'taxonomy' => 'career_level',
						'field'    => 'slug',
						'terms'    => array( $category->slug )
					)
				)
			);
			$postslist = get_posts( $args );
			if($postslist){
				foreach($postslist as $post){
				echo do_shortcode('[show_gd_mylist_btn add_icon="fa fa-user-plus" remove_icon="fa fa-user-plus" add_label="Follow" remove_label="Un Follow" item_id="'.$post->ID.'"]');
					echo '<div href="'.get_permalink($post->ID).'"><h3>'.$post->post_title.'</h3></div>';
					echo '<p>'.$post->post_content.'</p>';
					$post_meta = get_post_meta( $post->ID);
					//$career_courses = get_post_meta();				
					$career_courses = $post_meta['career_courses'];
					if($career_courses){
						echo'<h5>Courses List</h5>';
						foreach($career_courses as $career_course){
							echo '<div href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></div>';
						}
					}
				}
			}else{
				echo 'No Carrer found for '.$category->name.' LEVEL';
			}
			echo '</div>';
		}
	}
}
function wp_profile_header($atts) {
	$user_id = get_current_user_id();
	$userdata =get_userdata($user_id);
$image_url = get_avatar_url( $user_id);
$user_name = $userdata->data->display_name;
?>


<div class="tdc-row">
    <div class="wpb_row td-pb-row">
<div class="vc_column wpb_column vc_column_container tdc-column td-pb-span12">
    <div class="wpb_wrapper">
        <div class="wpb_wrapper wpb_text_column td_block_wrap td_block_wrap vc_column_text">
        <div class="td-fix-index">
        <div style="background-image: url('/wp-content/uploads/2019/10/background-student-group.png');">
<div class="image-container"><img class="user_img" src="<?php print_r($image_url); ?>"></div>
<h1 class="entry-title td-page-title" style="text-align: center;"><?php echo $user_name; ?></h1>
<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
<br>
<p></p>
</div>
<div class="dashboard-tabs-bar">
    <div class="tabs-inner-bar">
        <a class="tab-btn" href="<?php echo site_url().'/user-profile/'?>"><i class="fa fa-user"></i> Profile</a>
        <a class="tab-btn" href="<?php echo site_url().'/carrer-list/'?>"><i class="fa fa-user-plus"></i> Career Tracks</a>
        <a class="tab-btn" href="<?php echo site_url().'/profile-setting/'?>"><i class="fas fa-user-cog"></i> My Setting</a>
        <a class="tab-btn" href="<?php echo site_url().'/help/'?>"><i class="fas fa-question-circle"></i> Help</a>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
}
function wp_profile_form($atts) {
	
if(is_user_logged_in()){
if(isset($_POST['edit_profile'])){
	$fav_cat_arr = $_POST['fav_cat'];
	$fav_provider_arr = $_POST['fav_provider'];
	$fav_universities_arr = $_POST['fav_universities'];
	$full_name = $_POST['full_name'];
	$location = $_POST['location'];
	$higher_degree = $_POST['higher_degree'];
	$feild_study = $_POST['feild_study'];
	$job_title = $_POST['job_title'];
	$about_me = $_POST['about_me'];	
	if($fav_cat_arr){
	$fav_cat = implode(",",$fav_cat_arr);
	}
	if($fav_provider_arr){
	$fav_provider = implode(",",$fav_provider_arr);
	}
	if($fav_universities_arr){
	$fav_universities = implode(",",$fav_universities_arr);
	}
	$user_id = get_current_user_id();
	update_user_meta( $user_id, 'fav_cat', $fav_cat );
	update_user_meta( $user_id, 'fav_provider', $fav_provider );
	update_user_meta( $user_id, 'fav_universities', $fav_universities );
	update_user_meta( $user_id, 'full_name', $full_name );
	update_user_meta( $user_id, 'location', $location );
	update_user_meta( $user_id, 'higher_degree', $higher_degree );
	update_user_meta( $user_id, 'feild_study', $feild_study );
	update_user_meta( $user_id, 'job_title', $job_title );
	update_user_meta( $user_id, 'about_me', $about_me );
	wp_redirect( site_url().'/user-profile/' );
	exit;
}
$user_id = get_current_user_id();
$fav_cat = get_user_meta( $user_id, 'fav_cat', true);
$fav_provider = get_user_meta( $user_id, 'fav_provider', true );
$fav_universities = get_user_meta( $user_id, 'fav_universities', true);
$full_name = get_user_meta( $user_id, 'full_name',true);
$location = get_user_meta( $user_id, 'location',true);
$higher_degree = get_user_meta( $user_id, 'higher_degree',true );
$feild_study = get_user_meta( $user_id, 'feild_study', true );
$job_title = get_user_meta( $user_id, 'job_title', true);
$about_me = get_user_meta( $user_id, 'about_me',true );	
$get_user_meta = get_user_meta( $user_id);
$fav_cat_arr = explode(",",$fav_cat);
$fav_provider_arr = explode(",",$fav_provider);
$fav_universities_arr = explode(",",$fav_universities);

$fav_cat = get_category_by_slug( 'favorite-categories' );
$fav_cat_id = $fav_cat->term_id;
$fav_uni = get_category_by_slug( 'favorite-universities' );
$fav_uni_id = $fav_uni->term_id;
$fav_prov = get_category_by_slug( 'favorite-provider' );
$fav_prov_id = $fav_prov->term_id;

$fav_categories = get_terms('category', array('hide_empty' => 0, 'orderby' => 'ASC', 'parent' => $fav_cat_id ));
$fav_universities = get_terms('category', array('hide_empty' => 0, 'orderby' => 'ASC', 'parent' => $fav_uni_id ));
$fav_providers = get_terms('category', array('hide_empty' => 0, 'orderby' => 'ASC', 'parent' => $fav_prov_id ));

ob_start();
?>
<form role="form" action="" method="post" id="addmenu" method="post">
							<div class="tab">
							    <label><h3>Select Your Favorite Categories</h3></label>
							    <div class="form-row">
								<?php
										 foreach($fav_categories as $wcatTerms) {
      $children = get_categories( array ('taxonomy' => '10', 'parent' => $wcatTerms->term_id ));

			  if ( count($children) == 0 ) {
				  ?>
				  <div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="<?php echo $wcatTerms->name; ?>" <?php if(in_array("Art & Design",$fav_cat_arr)){?> checked="checked"<?php }?>><?php echo $wcatTerms->name; ?></label>
									</div>
								</div>
								<?php
				  
			  }
		 }
		 ?>
								
								<div class="form-group col-md-4">
								<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="Business" <?php if(in_array("Art & Design",$fav_cat_arr)){?> checked="checked"<?php }?>>Business</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="Engineering" <?php if(in_array("Art & Design",$fav_cat_arr)){?> checked="checked"<?php }?>> Engineering</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="Programming" <?php if(in_array("Programming",$fav_cat_arr)){?> checked="checked"<?php }?>>Programming</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="Data Science" <?php if(in_array("Data Science",$fav_cat_arr)){?> checked="checked"<?php }?>>Data Science</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="Web Development" <?php if(in_array("Web Development",$fav_cat_arr)){?> checked="checked"<?php }?>>Web Development</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="Blockchain" <?php if(in_array("Blockchain",$fav_cat_arr)){?> checked="checked"<?php }?>>Blockchain</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="IT & Software" <?php if(in_array("IT & Software",$fav_cat_arr)){?> checked="checked"<?php }?>>It & Software</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_cat[]" type="checkbox" value="Test & Certification" <?php if(in_array("Test & Certification",$fav_cat_arr)){?> checked="checked"<?php }?>>Test & Certification</label>
									</div>
									</div>
								</div>								
								</div>								
								<div class="tab">
								    <label><h3>Select Your Favorite Provider</h3></label>
								    <div class="form-row">
								    	<?php
								 foreach($fav_providers as $fav_provider) {
								$children = get_categories( array ('taxonomy' => '10', 'parent' => $fav_provider->term_id ));

							if ( count($children) == 0 ) {
									  ?>
									<div class="form-group col-md-4">
																	
													<div class="checkbox">
													  <label class="checkbox-inline"><input name="fav_provider[]" type="checkbox" value="<?php echo $fav_provider->name; ?>" <?php if(in_array("Courera",$fav_provider_arr)){?> checked="checked"<?php }?>><?php echo $fav_provider->name; ?></label>
													</div>
												</div>
									<?php
								  }
										 }
							?>										 
									
									<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_provider[]" type="checkbox" value="edX" <?php if(in_array("edX",$fav_provider_arr)){?> checked="checked"<?php }?>>edX</label>
									</div>
									</div>
									<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_provider[]" type="checkbox" value="FutureLearn" <?php if(in_array("FutureLearn",$fav_provider_arr)){?> checked="checked"<?php }?>>FutureLearn</label>
									</div>
									</div>
									<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_provider[]" type="checkbox" value="Udemy" <?php if(in_array("Udemy",$fav_provider_arr)){?> checked="checked"<?php }?>>Udemy</label>
									</div>
									</div>
									<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_provider[]" type="checkbox" value="Udocity" <?php if(in_array("Udocity",$fav_provider_arr)){?> checked="checked"<?php }?>> Udocity</label>
									</div>
									</div>
									<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_provider[]" type="checkbox" value="Swayam" <?php if(in_array("Swayam",$fav_provider_arr)){?> checked="checked"<?php }?>>Swayam</label>
									</div>
									</div>
								
								</div>
							</div>								
								<div class="tab">
								<label><h3>Select Your Favorite Universities</h3></label>
								<div class="form-row">
								
								
								<?php
								 foreach($fav_universities as $fav_university) {
								$children = get_categories( array ('taxonomy' => '10', 'parent' => $fav_university->term_id ));

							if ( count($children) == 0 ) {
									  ?>
									  	<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="<?php echo $fav_university->name; ?>" <?php if(in_array("Stanford",$fav_universities_arr)){?> checked="checked"<?php }?>><?php echo $fav_university->name; ?></label>
									</div>
								</div>
									  <?php
							}
								 }
								 ?>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="Howard" <?php if(in_array("Howard",$fav_universities_arr)){?> checked="checked"<?php }?>>Howard</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="MIT" <?php if(in_array("MIT",$fav_universities_arr)){?> checked="checked"<?php }?>>MIT</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="Jhon Hopkings" <?php if(in_array("Jhon Hopkings",$fav_universities_arr)){?> checked="checked"<?php }?>>Jhon Hopkings</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="Michigan" <?php if(in_array("Michigan",$fav_universities_arr)){?> checked="checked"<?php }?>>Michigan</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="Open University" <?php if(in_array("Open University",$fav_universities_arr)){?> checked="checked"<?php }?>>Open University</label>
									</div>	
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="Berkeley" <?php if(in_array("Berkeley",$fav_universities_arr)){?> checked="checked"<?php }?>>Berkeley</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="Pennsylvania" <?php if(in_array("Pennsylvania",$fav_universities_arr)){?> checked="checked"<?php }?>>Pennsylvania</label>
									</div>
									</div>
								<div class="form-group col-md-4">
									<div class="checkbox">
									  <label class="checkbox-inline"><input name="fav_universities[]" type="checkbox" value="UNSW" <?php if(in_array("UNSW",$fav_universities_arr)){?> checked="checked"<?php }?>>UNSW</label>
									</div>
								</div>
								</div>
								</div>
								<div class="tab">
								    <label><h3>More About You</h3></label>
								<div class="form-row">
								        
								<div class="form-group col-md-6">
									<label class="control-label" for="address">Full Name:</label>
									<input   placeholder="Full Name" style="background-color:white;" type="text" name="full_name" value="<?php if($full_name) echo $full_name; ?>" class="form-control" id="full_name" required>
								</div>								
								<div class="form-group col-md-6">
									<label class="control-label" for="address">Location:</label>
									<input   placeholder="Location" style="background-color:white;" type="text" name="location" value="<?php if($location) echo $location; ?>" class="form-control" id="location" required>
								</div>								
								<div class="form-group col-md-6">
									<label class="control-label" for="address">Higher Degree Obtained:</label>
									<input   placeholder="Higher Degree Obtained:" style="background-color:white;" type="text" name="higher_degree" value="<?php if($higher_degree) echo $higher_degree; ?>" class="form-control" id="higher_degree" required>
								</div>								
								<div class="form-group col-md-6">
									<label class="control-label" for="address">Feild Of Study:</label>
									<input   placeholder="Feild Of Study:" style="background-color:white;" type="text" name="feild_study" value="<?php if($feild_study) echo $feild_study; ?>" class="form-control" id="feild_study" required>
								</div>								
								<div class="form-group col-md-6">
									<label class="control-label" for="address">Job Title:</label>
									<input   placeholder="Enter Full Name here" style="background-color:white;" type="text" name="job_title" value="<?php if($job_title) echo $job_title; ?>" class="form-control" id="job_title" required>
								</div>
								<div class="form-group col-md-12">
									<label class="control-label" for="dish">About Me:</label>
									<textarea  placeholder="About Me" col="10" rows="8" style="background-color:white;" type="text" name="about_me" class="form-control" id="about_me"><?php if($about_me) echo $about_me; ?></textarea>
								</div>
								<input type="submit" name="edit_profile" value="Save Changes" class="btn btn-primary btn-block">
								</div>
								</div>

  <div class="row">
  <div class="col-md-12" style="text-align:right;">
    <button type="button" id="prevBtn" class="btn info" onclick="nextPrev(-1)">Previous</button>
    <button type="button" id="skipBtn" class="btn info" onclick="nextPrev(1)">Skip</button>
    <button type="button" id="nextBtn" class="btn info" onclick="nextPrev(1)">Next</button>
  </div>
  </div>

<!-- Circles which indicates the steps of the form: -->
<div class="row">
<div class="col-md-12" style="text-align:center;">
  <span class="step">1</span>
  <span class="step">2</span>
  <span class="step">3</span>
  <span class="step">4</span>
</div>
</div>
								
	</form>
	
	<style>
							/* Style the form */
#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

/* Style the input fields */
/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none; 
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

/* Mark the active step: */
.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
    background-color: #007db8;
}
input.btn.btn-primary.btn-block {
    width: auto;
    float: right;
    margin-left: 10px;
    background: #4db2ec;
    padding: 8px 15px;
    border-radius: 0;

}
.info {
  border-color: #2196F3;
  color: dodgerblue
}
.btn {
  border: 2px solid black;
  background-color: white;
  color: black;
 padding: 2px 25px;
  font-size: 16px;
  cursor: pointer;
}
.info:hover {
  background: #2196F3;
  color: white;
}
							</style>
							
<script>
							
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("skipBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").style.display = "none"
    document.getElementById("skipBtn").style.display = "none"
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}

							</script>
							
	<?php
	
}else{	
	echo 'Please login first<a href="/membership-login/">Login</a>';
	//wp_redirect( home_url() ); exit;
}
return ob_get_clean();
}
function wp_profile_script_back_css() {

}

function wp_profile_script_front_css() {
		/* CSS */
        wp_register_style('wp_profile_style', plugins_url('css/wp_profile.css',__FILE__));
        wp_enqueue_style('wp_profile_style');
        wp_register_style('bootstrap_style', plugins_url('css/bootstrap.min.css',__FILE__));
        wp_enqueue_style('bootstrap_style');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function wp_profile_script_back_js() {
	
}



function wp_profile_script_front_js() {
 	
			   
        wp_register_script('wp_profile_script', plugins_url('js/wp_profile.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('wp_profile_script');
        wp_register_script('bootstrap_script', plugins_url('js/bootstrap.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('bootstrap_script');

}
