jQuery(document).ready(function($) {
    var BUTTON = "#mylist_btn_",
        uriAjax = gdMyListAjax.ajaxurl,
        boxList = gdMyListAjax.boxList,
        loading_icon = gdMyListAjax.loading_icon,
        button = gdMyListAjax.button,
        nonce = gdMyListAjax.nonce,
        buttonHtml = "";

    function createBtn() {
        0 < $(".js-item-mylist").length && $.get(button, function(source) {
            buttonHtml = source, $(".js-item-mylist").each(function() {
                var itemId = BUTTON + $(this).data("id"),
                    nameVar = "myListButton" + $(this).data("id"),
                    data = eval(nameVar);
                renderTemplate(itemId, source, data)
            })
        })
    }

    function showLoading(t) {
        data = $.parseJSON('{"showLoading": {"icon": "' + loading_icon + '"}}'), renderTemplate(t, buttonHtml, data)
    }

    function renderTemplate(t, a, n) {
        var e = Handlebars.compile(a)(n);
        $(t).html(e)
    }
    "undefined" != typeof myListData && $.get(boxList, function(t) {
        renderTemplate("#myList_list", t, myListData)
    }), createBtn(), $("body").on("click", ".js-gd-add-mylist", function() {
        var t = $(this).data("postid"),
            a = $(this).data("userid"),
            n = BUTTON + t;
        showLoading(n), $.ajax({
            type: "POST",
            dataType: "json",
            url: uriAjax,
            data: {
                action: "gd_add_mylist",
                itemId: t,
                userId: a,
                nonce: nonce
            }
        }).done(function(t) {
            renderTemplate(n, buttonHtml, t)
        })
    }), $("body").on("click", ".js-gd-remove-mylist", function() {
        var a = $(this).data("postid"),
            t = $(this).data("userid"),
            n = $(this).data("styletarget"),
            e = BUTTON + a;
        showLoading(e), $.ajax({
            type: "POST",
            dataType: "json",
            url: uriAjax,
            data: {
                action: "gd_remove_mylist",
                itemId: a,
                userId: t,
                nonce: nonce
            }
        }).done(function(t){
            "mylist" == n ? $("#mylist-" + a).closest(".gd-mylist-box").fadeOut(500) : renderTemplate(e, buttonHtml, t)
        })
    }), $("body").on("click", ".enroll_course", function() {
        var a = $(this).data("postid"),
            t = $(this).data("userid"),
            n = $(this).data("styletarget"),
            e = BUTTON + a;
        showLoading(e), $.ajax({
            type: "POST",
            dataType: "json",
            url: uriAjax,
            data: {
                action: "gd_enroll_course",
                itemId: a,
                userId: t,
                nonce: nonce
            }
        }).done(function(t) {
			$("#enrolllist-"+ a).attr('checked', true);
			$("#enrolllist-"+ a).attr("disabled", 'disabled');
			$("#mylist-"+ a).attr("disabled", 'disabled');
			$("#mylist-"+ a).removeClass("js-gd-remove-mylist");
			$("#completelist-"+ a).removeAttr( "disabled");
			$("#enrolllist-"+ a).closest(".enroll").html('<span class="btn-success">Enrolled</span>');
        })
    }), $("body").on("click", ".complete_course", function() {
        var a = $(this).data("postid"),
            t = $(this).data("userid"),
            n = $(this).data("styletarget"),
            e = BUTTON + a;
        showLoading(e), $.ajax({
            type: "POST",
            dataType: "json",
            url: uriAjax,
            data: {
                action: "gd_complete_course",
                itemId: a,
                userId: t,
                nonce: nonce
            }
        }).done(function(t) {
			$("#completelist-"+ a).attr('checked', true);
			$("#completelist-"+ a).attr("disabled", 'disabled');
			$("#completelist-"+ a).closest(".complete").html('<span class="btn-success">Completed</span>');
        })
    }), $("body").on("click", ".js-gd-share-post", function() {
        var a = $(this).data("postid");
           $("#td-post-share-"+a).toggle("slow");
    })
});