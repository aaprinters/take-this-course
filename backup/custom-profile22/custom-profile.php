<?php
/*
Plugin Name: custom profile form
Description: custom profile form
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

add_action('wp_enqueue_scripts', 'wp_profile_script_front_css');
add_action('wp_enqueue_scripts', 'wp_profile_script_front_js');
add_action('admin_enqueue_scripts', 'wp_profile_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {
   add_shortcode('edit_profile', 'wp_profile_form');
   add_shortcode('profile_header', 'wp_profile_header');
}
function wp_profile_form($atts) {
	$user_id = get_current_user_id();
	$userdata =get_userdata($user_id);
$image_url = get_avatar_url( $user_id);
?><pre><?php print_r($userdata); ?> </pre> <?php
?><pre><?php print_r($image_url); ?> </pre> <?php
}
function wp_profile_form($atts) {
	
if(is_user_logged_in()){
if(isset($_POST['edit_profile'])){
$fav_cat_arr = $_POST['fav_cat'];
$fav_provider_arr = $_POST['fav_provider'];
$fav_universities_arr = $_POST['fav_universities'];
$full_name = $_POST['full_name'];
$location = $_POST['location'];
$higher_degree = $_POST['higher_degree'];
$feild_study = $_POST['feild_study'];
$job_title = $_POST['job_title'];
$about_me = $_POST['about_me'];	
$fav_cat = implode(",",$fav_cat_arr);
$fav_provider = implode(",",$fav_provider_arr);
$fav_universities = implode(",",$fav_universities_arr);

$user_id = get_current_user_id();
update_user_meta( $user_id, 'fav_cat', $fav_cat );
update_user_meta( $user_id, 'fav_provider', $fav_provider );
update_user_meta( $user_id, 'fav_universities', $fav_universities );
update_user_meta( $user_id, 'full_name', $full_name );
update_user_meta( $user_id, 'location', $location );
update_user_meta( $user_id, 'higher_degree', $higher_degree );
update_user_meta( $user_id, 'feild_study', $feild_study );
update_user_meta( $user_id, 'job_title', $job_title );
update_user_meta( $user_id, 'about_me', $about_me );
}
$user_id = get_current_user_id();
$fav_cat = get_user_meta( $user_id, 'fav_cat', true);
$fav_provider = get_user_meta( $user_id, 'fav_provider', true );
$fav_universities = get_user_meta( $user_id, 'fav_universities', true);
$full_name = get_user_meta( $user_id, 'full_name',true);
$location = get_user_meta( $user_id, 'location',true);
$higher_degree = get_user_meta( $user_id, 'higher_degree',true );
$feild_study = get_user_meta( $user_id, 'feild_study', true );
$job_title = get_user_meta( $user_id, 'job_title', true);
$about_me = get_user_meta( $user_id, 'about_me',true );	
$get_user_meta = get_user_meta( $user_id);
$fav_cat_arr = explode(",",$fav_cat);
$fav_provider_arr = explode(",",$fav_provider);
$fav_universities_arr = explode(",",$fav_universities);

ob_start();
?>
<form role="form" action="" method="post" id="addmenu" method="post">
								<div class="form-group">
								<div class="alert alert-success">
								</div>								
								</div>
							<div class="tab">
								<div class="form-group">
								<label><h3>Select Your Favorite Categories</h3></label>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="Art & Design" <?php if(in_array("Art & Design",$fav_cat_arr)){?> checked="checked"<?php }?>>Art & Design</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="Business" <?php if(in_array("Art & Design",$fav_cat_arr)){?> checked="checked"<?php }?>>Business</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="Engineering" <?php if(in_array("Art & Design",$fav_cat_arr)){?> checked="checked"<?php }?>> Engineering</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="Programming" <?php if(in_array("Programming",$fav_cat_arr)){?> checked="checked"<?php }?>>Programming</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="Data Science" <?php if(in_array("Data Science",$fav_cat_arr)){?> checked="checked"<?php }?>>Data Science</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="Web Development" <?php if(in_array("Web Development",$fav_cat_arr)){?> checked="checked"<?php }?>>Web Development</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="Blockchain" <?php if(in_array("Blockchain",$fav_cat_arr)){?> checked="checked"<?php }?>>Blockchain</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="IT & Software" <?php if(in_array("IT & Software",$fav_cat_arr)){?> checked="checked"<?php }?>>It & Software</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_cat[]" type="checkbox" value="Test & Certification" <?php if(in_array("Test & Certification",$fav_cat_arr)){?> checked="checked"<?php }?>>Test & Certification</label>
									</div>
								</div>								
								</div>								
								<div class="tab">
								<div class="form-group">
								<label><h3>Select Your Favorite Provider</h3></label>
									<div class="checkbox">
									  <label><input name="fav_provider[]" type="checkbox" value="Courera" <?php if(in_array("Courera",$fav_provider_arr)){?> checked="checked"<?php }?>>Courera</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_provider[]" type="checkbox" value="edX" <?php if(in_array("edX",$fav_provider_arr)){?> checked="checked"<?php }?>>edX</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_provider[]" type="checkbox" value="FutureLearn" <?php if(in_array("FutureLearn",$fav_provider_arr)){?> checked="checked"<?php }?>>FutureLearn</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_provider[]" type="checkbox" value="Udemy" <?php if(in_array("Udemy",$fav_provider_arr)){?> checked="checked"<?php }?>>Udemy</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_provider[]" type="checkbox" value="Udocity" <?php if(in_array("Udocity",$fav_provider_arr)){?> checked="checked"<?php }?>> Udocity</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_provider[]" type="checkbox" value="Swayam" <?php if(in_array("Swayam",$fav_provider_arr)){?> checked="checked"<?php }?>>Swayam</label>
									</div>
								</div>								
							</div>								
								<div class="tab">
								<div class="form-group">
								<label><h3>Select Your Favorite Universities</h3></label>
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="Stanford" <?php if(in_array("Stanford",$fav_universities_arr)){?> checked="checked"<?php }?>>Stanford</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="Howard" <?php if(in_array("Howard",$fav_universities_arr)){?> checked="checked"<?php }?>>Howard</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="MIT" <?php if(in_array("MIT",$fav_universities_arr)){?> checked="checked"<?php }?>>MIT</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="Jhon Hopkings" <?php if(in_array("Jhon Hopkings",$fav_universities_arr)){?> checked="checked"<?php }?>>Jhon Hopkings</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="Michigan" <?php if(in_array("Michigan",$fav_universities_arr)){?> checked="checked"<?php }?>>Michigan</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="Open University" <?php if(in_array("Open University",$fav_universities_arr)){?> checked="checked"<?php }?>>Open University</label>
									</div>			
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="Berkeley" <?php if(in_array("Berkeley",$fav_universities_arr)){?> checked="checked"<?php }?>>Berkeley</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="Pennsylvania" <?php if(in_array("Pennsylvania",$fav_universities_arr)){?> checked="checked"<?php }?>>Pennsylvania</label>
									</div>
									<div class="checkbox">
									  <label><input name="fav_universities[]" type="checkbox" value="UNSW" <?php if(in_array("UNSW",$fav_universities_arr)){?> checked="checked"<?php }?>>UNSW</label>
									</div>
								</div>
								</div>
								<div class="tab">
								<div class="form-group">
									<label class="control-label" for="address">Full Name:</label>
									<input   placeholder="Enter Full Name here" style="background-color:white;" type="text" name="full_name" value="<?php if($full_name) echo $full_name; ?>" class="form-control" id="full_name" required>
								</div>								
								<div class="form-group">
									<label class="control-label" for="address">Location:</label>
									<input   placeholder="Enter Full Name here" style="background-color:white;" type="text" name="location" value="<?php if($location) echo $location; ?>" class="form-control" id="location" required>
								</div>								
								<div class="form-group">
									<label class="control-label" for="address">Higher Degree Obtained:</label>
									<input   placeholder="Enter Full Name here" style="background-color:white;" type="text" name="higher_degree" value="<?php if($higher_degree) echo $higher_degree; ?>" class="form-control" id="higher_degree" required>
								</div>								
								<div class="form-group">
									<label class="control-label" for="address">Feild Of Study:</label>
									<input   placeholder="Enter Full Name here" style="background-color:white;" type="text" name="feild_study" value="<?php if($feild_study) echo $feild_study; ?>" class="form-control" id="feild_study" required>
								</div>								
								<div class="form-group">
									<label class="control-label" for="address">Job Title:</label>
									<input   placeholder="Enter Full Name here" style="background-color:white;" type="text" name="job_title" value="<?php if($job_title) echo $job_title; ?>" class="form-control" id="job_title" required>
								</div>
								<div class="form-group">
									<label class="control-label" for="dish">About Me:</label>
									<textarea  placeholder="Enter Description of your Dish here" col="10" rows="8" style="background-color:white;" type="text" name="about_me" class="form-control" id="about_me" required><?php if($about_me) echo $about_me; ?></textarea>
								</div>
								<input type="submit" name="edit_profile" value="Save Changes" class="btn btn-primary btn-block">
								</div>
							<div style="overflow:auto;">
  <div style="float:right;">
    <button type="button" id="prevBtn" class="btn info" onclick="nextPrev(-1)">Previous</button>
    <button type="button" id="skipBtn" class="btn info" onclick="nextPrev(1)">Skip</button>
    <button type="button" id="nextBtn" class="btn info" onclick="nextPrev(1)">Next</button>
  </div>
</div>

<!-- Circles which indicates the steps of the form: -->
<div style="text-align:center;margin-top:40px;">
  <span class="step"></span>
  <span class="step"></span>
  <span class="step"></span>
  <span class="step"></span>
</div>
								
							</form>
							
							<style>
							/* Style the form */
#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

/* Style the input fields */
input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none; 
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

/* Mark the active step: */
.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
    background-color: #007db8;
}
input.btn.btn-primary.btn-block {
    width: 25%;
    float: right;
    margin-left: 25px;
    line-height: initial;
    background: #007db8;
}
.info {
  border-color: #2196F3;
  color: dodgerblue
}
.btn {
  border: 2px solid black;
  background-color: white;
  color: black;
 padding: 2px 25px;
  font-size: 16px;
  cursor: pointer;
}
.info:hover {
  background: #2196F3;
  color: white;
}
							</style>
							
							<script>
							
							var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("skipBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").style.display = "none"
    document.getElementById("skipBtn").style.display = "none"
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}
							</script>
							
	<?php
	
}else{	
	echo 'Please login first<a href="https://web500.co.uk/membership-login/">Login</a>';
	//wp_redirect( home_url() ); exit;
}
return ob_get_clean();
}
function wp_profile_script_back_css() {

}

function wp_profile_script_front_css() {
		/* CSS */
        wp_register_style('wp_profile_style', plugins_url('css/wp_profile.css',__FILE__));
        wp_enqueue_style('wp_profile_style');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function wp_profile_script_back_js() {
	
}



function wp_profile_script_front_js() {
 	
			   
        wp_register_script('wp_profile_script', plugins_url('js/wp_profile.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('wp_profile_script');

}
