<?php
/**
 * Template Name: Register Telent edit
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(); 

?>
			
<?php
//$emptyRemoved = remove_empty($linksArray);

$current_user_id = get_current_user_id();
$talent_post_id = get_user_meta( $current_user_id, 'talent_post_id', true );
$second_post_link = get_post_meta($talent_post_id,'talent_post_id_second');

$user_info = get_userdata($current_user_id);
$user_role_arr = $user_info->roles;
 $user_role = $user_role_arr[0];
  if($user_role == 'talent'){

			if (isset( $_POST['register_telent'] )) {
						//Get current post ID
						
						$current_user_id = get_current_user_id();
						$talent_post_id = get_user_meta( $current_user_id, 'talent_post_id', true );

						//set gallery images
						$gallery_images = $_POST['gallery_images'];
						$gallery_vid_custom = $_POST['upload_videos'];
						update_post_meta( $talent_post_id, 'gallery_images', $gallery_images );
						update_post_meta( $talent_post_id, 'video_custom', $gallery_vid_custom );

						$password = $_POST['password'];
						$firstname = $_POST['firstname'];
						$lastname = $_POST['lastname'];
						$city = $_POST['city'];
						$street = $_POST['street'];
						$email = $_POST['email'];
						$phone = $_POST['phone'];
						$user_pass = $_POST['password'];
						$your_bio = $_POST['your_bio'];
						$slected_category = $_POST['category'];
						$category = $_POST['category'];
						$experience = $_POST['experience'];
						$assignment = $_POST['assignments'];
						$description = $_POST['description'];
						$talent_cv = $_POST['talent_cv'];
						$talent_video_url = $_POST['video_url'];
						$address = $street.' '.$city;
						$full_name = $firstname.' '.$lastname;
						$multi_category = $_POST['multi_category'];
						//Music
						$tel_music_category = $_POST['music_category'];
						$tel_band = $_POST['band'];
						$tel_genre = $_POST['genre'];
						$tel_crew = $_POST['crew'];
						$tel_dj_experience = $_POST['dj_experience'];
						$tel_singer_x_solo_exp = $_POST['singer_x_solo_exp'];
						$tel_instrumentals_type = $_POST['instrumentals_type'];
						$tel_played_number_of_years = $_POST['played_number_of_years'];
						$tel_music_gender = $_POST['music_gender'];

						//Actor
						$tel_actor_age = $_POST['actor_age'];
						$tel_actor_gender = $_POST['actor_gender'];
						$tel_actor_height = $_POST['actor_height'];
						$tel_actor_weight = $_POST['actor_weight'];
						$tel_actor_hair_color = $_POST['actor_hair_color'];
						$tel_actor_language = $_POST['actor_language'];
						$tel_actor_experience = $_POST['actor_experience'];
						$tel_email = $_POST['email'];

						//film_
					

						//Model
						$tel_model_name = $_POST['model_name'];
						$tel_model_age = $_POST['model_age'];
						$tel_model_gender = $_POST['model_gender'];
						$tel_model_height = $_POST['model_height'];
						$tel_model_weight = $_POST['model_weight'];
						$tel_model_exprience = $_POST['model_exprience'];
						
						//statist
						$tel_statist_age = $_POST['statist_age'];
						$tel_statist_gender = $_POST['statist_gender'];
						$tel_statist_height = $_POST['statist_height'];
						$statist_hair_color = $_POST['statist_hair_color'];
						$tel_statist_weight = $_POST['statist_weight'];
						$statist_dialect = $_POST['statist_dialect'];
						$statist_language = $_POST['statist_language'];
						$tel_statist_exprience = $_POST['statist_experience'];
						$film_category = $_POST['film_category'];
						
						//Refrence List
						$tel_company_name = $_POST['company_name'];
						$tel_designation = $_POST['designation'];
						$tel_job_profile = $_POST['job_profile'];
						$tel_job_duration_from = $_POST['job_duration_from'];
						$tel_job_duration_to = $_POST['job_duration_to'];


						//Voiceover
						$tel_voiceover_gender = $_POST['voiceover_gender'];
						$tel_voiceover_height = $_POST['voiceover_height'];
						$voiceover_hair_color = $_POST['voiceover_hair_color'];
						$tel_voiceover_weight = $_POST['voiceover_weight'];
						$voiceover_dialect = $_POST['voiceover_dialect'];
						$voiceover_language = $_POST['voiceover_language'];
						$tel_voiceover_exprience_arr = $_POST['voiceover_experience'];
						$tel_voiceover_exprience = implode(",",$tel_voiceover_exprience_arr);
						$voiceover_description = $_POST['voiceover_description'];
						$talent_audio_get = $_POST['talent_audio'];
						$talent_audio_arr = explode(',',$talent_audio_get);
						$talent_audio = $talent_audio_arr[0];
						$film_category = $_POST['film_category'];
						//// Here is multi category check
						$multi_category_count = count($multi_category);
						$talent_post_id = get_user_meta( $current_user_id, 'talent_post_id', true );
						if($multi_category[1]){

					foreach($multi_category as $category){
						if($film_category == 'statist'){
							$tel_age = $tel_statist_age;
							$tel_gender = $tel_statist_gender;
							$tel_height = $tel_statist_height;
							$tel_weight = $tel_statist_weight;
							$tel_hair_color = $statist_hair_color;
							$tel_dialect = $statist_dialect;
							$tel_language = $statist_language;
 							$tel_experience = $tel_statist_exprience;
						}
						if($film_category == 'voiceover'){
							$tel_age = $tel_statist_age;
							$tel_gender = $tel_voiceover_gender;
							$tel_height = $tel_voiceover_height;
							$tel_weight = $tel_voiceover_weight;
							$tel_hair_color = $statist_hair_color;
							$tel_language = $statist_language;
 							$tel_experience = $tel_voiceover_exprience;
 							$tel_experienc_description = $voiceover_description;
 							$tel_audio = $talent_audio;
						}
						/* if($film_category == 'actor' || $film_category == 'model'){
							$category = $film_category;
						} */
						if($category == 'sports'){
							$category = 'culture';
						}
						// Dancer
						$dance_experience = $_POST['dance_experience'];
						$dance_experience_description = $_POST['dance_experience_description'];
						$dancer_type = $_POST['dancer_type'];
			/* 			$post_type_check = get_post_type( $talent_post_id );

					if($category == $post_type_check){
						$talent_post_id = get_user_meta( $current_user_id, 'talent_post_id', true );
					}else{
						$my_post = array(
						  'post_title'    		 => $full_name,
						  'post_content' 		 => '',
						  'post_status'          => 'publish',				  
						  'post_type'            => $category
						 );
						// Insert the post into the database
						$talent_post_id_second = wp_insert_post( $my_post );
						?><pre><?php print_r($talent_post_id_second); ?></pre> <?php

						update_post_meta($talent_post_id,'talent_post_id_second',$talent_post_id_second);
						$talent_post_id = $talent_post_id_second;
					} */
					
					if($talent_post_id){
					update_post_meta( $talent_post_id, 'talent_cv', $talent_cv );
						//Image
						$talent_image = $_POST['talent_image'];
						$tel_talent_image_arr = explode(',',$talent_image);
						$talent_image = $tel_talent_image_arr[0];
						if($talent_image){
						//Feature Image Code
						
						// Add Featured Image to Post
						$image_url        = $talent_image; // Define the image URL here
						$image_name       = 'telent.png';
						$upload_dir       = wp_upload_dir(); // Set upload folder
						$image_data       = file_get_contents($image_url); // Get image data
						$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
						$filename         = basename( $unique_file_name ); // Create image file name

						// Check folder permission and define file location
						if( wp_mkdir_p( $upload_dir['path'] ) ) {
						    $file = $upload_dir['path'] . '/' . $filename;
						} else {
						    $file = $upload_dir['basedir'] . '/' . $filename;
						}

						// Create the image  file on the server
						file_put_contents( $file, $image_data );

						// Check image file type
						$wp_filetype = wp_check_filetype( $filename, null );

						// Set attachment data
						$attachment = array(
						    'post_mime_type' => $wp_filetype['type'],
						    'post_title'     => sanitize_file_name( $filename ),
						    'post_content'   => '',
						    'post_status'    => 'inherit'
						);

						// Create the attachment
						$attach_id = wp_insert_attachment( $attachment, $file, $talent_post_id );

						// Include image.php
						require_once(ABSPATH . 'wp-admin/includes/image.php');

						// Define attachment metadata
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

						// Assign metadata to attachment
						wp_update_attachment_metadata( $attach_id, $attach_data );

						// And finally assign featured image to post
						set_post_thumbnail( $talent_post_id, $attach_id );
						//Feature Image Code Ends	
						
						}
						//Default Image if no image Uplaoded
						else{
							 // if($category == 'culture'){	
								// set_post_thumbnail( $talent_post_id, 4306 );	
							 // }
							 // if($category == 'actor'){	
								// set_post_thumbnail( $talent_post_id, 4307 );	
							 // }
							 // if($category == 'music'){	
								// set_post_thumbnail( $talent_post_id, 4309 );	
							 // } 
							 // if($category == 'dancer'){	
								// set_post_thumbnail( $talent_post_id, 4310 );	
							 // } 
							 // if($category == 'model'){	
								// set_post_thumbnail( $talent_post_id, 4308 );
							 // }
							
							$gender_check = '';	
							
							//Male
							if($tel_music_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_actor_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_model_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_statist_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_voiceover_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_gender == 'male'){	
								$gender_check = 'male';
							 }
							
							//Female
							if($tel_music_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_actor_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_model_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_statist_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_voiceover_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_gender == 'female'){	
								$gender_check = 'female';
							 }
							 
							 //Condition
							 if ($gender_check == 'male') {
							 	set_post_thumbnail( $talent_post_id, 5210 );
							 }
							 else if ($gender_check == 'female') {
							 	set_post_thumbnail( $talent_post_id, 5211 );
							 }							 	
						}
						//Default Image if no image Uplaoded Ends
											
						//Get Post Title 
						//$tel_title = $firstname." ".$lastname;
						// Update post title
						$my_post_tel = array(
						      'ID'           => $talent_post_id,
						      'post_title'   => $full_name,
							  'post_content' => $your_bio,
						 );

						// Update the post into the database
						wp_update_post( $my_post_tel );					
						update_post_meta( $talent_post_id, 'city', $city );


						//Update Current User
						
						$user_id = get_current_user_id();
						//$website = 'http://example.com';
						$user_data = wp_update_user( array(
							'ID' => $user_id, 
							'first_name' => $firstname,
							'last_name' => $lastname,
							'user_pass' => $user_pass,
							'user_email' => $email ) );
						 
						if ( is_wp_error( $user_data ) ) {
						    // There was an error; possibly this user doesn't exist.
						   // echo 'Error.';
						} else {
						   echo 'Success!';
						   echo 'User profile updated.';
						}

						//Refrence
						
						//Culture
						if($category == 'culture'){
						
						$tel_culture_expertise = $_POST['culture_expertise'];
						$tel_culture_experience = $_POST['culture_experience'];
						$tel_culture_experience_description = $_POST['culture_experience_description'];
						$tel_cul_gender = $_POST['cul_gender'];

						
						update_post_meta( $talent_post_id, 'gender', $tel_cul_gender );
						
						if($talent_video_url != NULL) {
							update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
						}

						update_post_meta( $talent_post_id, 'expertise', $tel_culture_expertise );
						update_post_meta( $talent_post_id, 'experience', $tel_culture_experience );
						update_post_meta( $talent_post_id, 'experince_description', $tel_culture_experience_description );
						update_post_meta( $talent_post_id, 'phone', $phone );
						update_post_meta( $talent_post_id, 'email', $email );
						update_post_meta( $talent_post_id, 'address', $address );
						update_post_meta( $talent_post_id, 'assignment', $assignment );
						}
						 else if($category == 'actor'){	
						update_post_meta( $talent_post_id, 'age', $tel_actor_age );
						update_post_meta( $talent_post_id, 'gender', $tel_actor_gender );
						update_post_meta( $talent_post_id, 'height', $tel_actor_height );
						update_post_meta( $talent_post_id, 'weight', $tel_actor_weight );
						update_post_meta( $talent_post_id, 'hair_color', $tel_actor_hair_color );
						update_post_meta( $talent_post_id, 'language', $tel_actor_language );
						update_post_meta( $talent_post_id, 'city', $data_city );
						update_post_meta( $talent_post_id, 'experience', $tel_actor_experience );
						update_post_meta( $talent_post_id, 'phone', $phone );
						update_post_meta( $talent_post_id, 'email', $email );
						update_post_meta( $talent_post_id, 'address', $address );
						update_post_meta( $talent_post_id, 'assignment', $assignment );
						if($talent_video_url != NULL) {
							update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
						}
						}
						 elseif($category == 'music'){
							$music_category = $_POST['music_category'];
							$band_category = $_POST['band_category'];
													
							update_post_meta( $talent_post_id, 'music_gender', $tel_music_gender );


							if($music_category == 'band'){
							$music_categoies = array($music_category,$band_category);
							$tel_band_genre = $_POST['band_genre'];
							$tel_founded = $_POST['founded'];
							$tel_repertoire = $_POST['repertoire'];
							$tel_crew = $_POST['crew'];
							
							update_post_meta( $talent_post_id, 'band_type', $band_category );
							update_post_meta( $talent_post_id, 'genre', $tel_band_genre );
							update_post_meta( $talent_post_id, 'crew', $tel_crew );
							update_post_meta( $talent_post_id, 'repertoire', $tel_repertoire );
							update_post_meta( $talent_post_id, 'founded', $tel_founded );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							wp_set_object_terms( $talent_post_id, $music_categoies, 'music_category');

							}elseif($music_category == 'dj'){

							$dj_experience = $_POST['dj_experience'];
							$dj_description = $_POST['dj_description'];
							update_post_meta( $talent_post_id, 'experience', $dj_experience );
							update_post_meta( $talent_post_id, 'experince_description', $dj_description );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							wp_set_object_terms( $talent_post_id, $music_category, 'music_category');
								
							}elseif($music_category == 'singer'){
								$singer_experience = $_POST['singer_experience'];
								$singer_description = $_POST['singer_description'];
								update_post_meta( $talent_post_id, 'experience', $singer_experience );
								update_post_meta( $talent_post_id, 'experince_description', $singer_description );
								update_post_meta( $talent_post_id, 'phone', $phone );
								update_post_meta( $talent_post_id, 'email', $email );
								update_post_meta( $talent_post_id, 'address', $address );
								update_post_meta( $talent_post_id, 'assignment', $assignment );
								
								if($talent_video_url != NULL) {
									update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
								}
								wp_set_object_terms( $talent_post_id, $music_category, 'music_category');
							}elseif($music_category == 'instrumentals'){
								$type_instrument = $_POST['type_instrument'];
								$instrumental_experience = $_POST['instrumental_experience'];
								$instrumental_description = $_POST['instrumental_description'];
								$tel_crew = $_POST['crew'];
								update_post_meta( $talent_post_id, 'type_instrument', $type_instrument );
								update_post_meta( $talent_post_id, 'experience', $instrumental_experience );
								update_post_meta( $talent_post_id, 'experince_description', $instrumental_description );
								update_post_meta( $talent_post_id, 'phone', $phone );
								update_post_meta( $talent_post_id, 'email', $email );
								update_post_meta( $talent_post_id, 'address', $address );
								update_post_meta( $talent_post_id, 'assignment', $assignment );
								if($talent_video_url != NULL) {
									update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
								}
								wp_set_object_terms( $talent_post_id, $music_category, 'music_category');
							}
							//wp_set_object_terms( $talent_post_id, $band_category, 'music_category');

						}
					
						 elseif($category == 'film'){	
							update_post_meta( $talent_post_id, 'age', $tel_age );
							update_post_meta( $talent_post_id, 'gender', $tel_gender );
							update_post_meta( $talent_post_id, 'height', $tel_height );
							update_post_meta( $talent_post_id, 'weight', $tel_weight );
							update_post_meta( $talent_post_id, 'hair_color', $tel_hair_color );
							update_post_meta( $talent_post_id, 'dialect', $tel_dialect );
							update_post_meta( $talent_post_id, 'language', $tel_language );
							update_post_meta( $talent_post_id, 'experience', $tel_experience );
							update_post_meta( $talent_post_id, 'talent_audio', $talent_audio );
							update_post_meta( $talent_post_id, 'experience_description', $tel_experienc_description );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							wp_set_object_terms( $talent_post_id, $film_category, 'film_category');
						 }
						 elseif($category == 'dancer'){
							update_post_meta( $talent_post_id, 'dance_experience', $dance_experience );
							update_post_meta( $talent_post_id, 'dance_experience_description', $dance_experience_description );
							update_post_meta( $talent_post_id, 'dancer_type', $dancer_type );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							wp_set_object_terms( $talent_post_id, $dancer_type, 'dancer_category');
						 } 
						elseif($category == 'model'){	
						 //Model
							update_post_meta( $talent_post_id, 'age', $tel_model_age );
							update_post_meta( $talent_post_id, 'gender', $tel_model_gender );
							update_post_meta( $talent_post_id, 'height', $tel_model_height );
							update_post_meta( $talent_post_id, 'mweight', $tel_model_weight );
							update_post_meta( $talent_post_id, 'exprience', $tel_model_exprience );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							update_post_meta( $talent_post_id, 'assignment', $assignment );
						}
						if($slected_category == 'sports'){
							$tel_sports_expertise = $_POST['sports_expertise'];
							$tel_sports_experience = $_POST['sports_experience'];
							$tel_sports_experience_description = $_POST['sports_experience_description'];
							$tel_cul_gender = $_POST['sports_gender'];
							$cul_sports_age = $_POST['sports_age'];
							if($talent_video_url != NULL) {
								$tel_cul_video_url = $talent_video_url;
							}

							$tel_cul_image_url = $talent_image;
							update_post_meta( $talent_post_id, 'gender', $tel_cul_gender );
							update_post_meta( $talent_post_id, 'age', $cul_sports_age );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							update_post_meta( $talent_post_id, 'expertise', $tel_sports_expertise );
							update_post_meta( $talent_post_id, 'experience', $tel_sports_experience );
							update_post_meta( $talent_post_id, 'experince_description', $tel_sports_experience_description );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							wp_set_object_terms( $talent_post_id, $slected_category, 'culture_categories');
						}
					}
				}
			}else{
					if($film_category == 'statist'){
							$tel_age = $tel_statist_age;
							$tel_gender = $tel_statist_gender;
							$tel_height = $tel_statist_height;
							$tel_weight = $tel_statist_weight;
							$tel_hair_color = $statist_hair_color;
							$tel_dialect = $statist_dialect;
							$tel_language = $statist_language;
 							$tel_experience = $tel_statist_exprience;
						}
						if($film_category == 'voiceover'){
							$tel_age = $tel_statist_age;
							$tel_gender = $tel_voiceover_gender;
							$tel_height = $tel_voiceover_height;
							$tel_weight = $tel_voiceover_weight;
							$tel_hair_color = $statist_hair_color;
							$tel_language = $statist_language;
 							$tel_experience = $tel_voiceover_exprience;
 							$tel_experienc_description = $voiceover_description;
 							$tel_audio = $talent_audio;
						}
						if($film_category == 'actor' || $film_category == 'model'){
							$category = $film_category;
						}
						if($category == 'sports'){
							$category = 'culture';
						}
						// Dancer
						$dance_experience = $_POST['dance_experience'];
						$dance_experience_description = $_POST['dance_experience_description'];
						$dancer_type = $_POST['dancer_type'];
					
					if($talent_post_id){
						update_post_meta( $talent_post_id, 'talent_cv', $talent_cv );
						//Image
						$talent_image = $_POST['talent_image'];
						$tel_talent_image_arr = explode(',',$talent_image);
						$talent_image = $tel_talent_image_arr[0];
						if($talent_image){
						//Feature Image Code
						
						// Add Featured Image to Post
						$image_url        = $talent_image; // Define the image URL here
						$image_name       = 'telent.png';
						$upload_dir       = wp_upload_dir(); // Set upload folder
						$image_data       = file_get_contents($image_url); // Get image data
						$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
						$filename         = basename( $unique_file_name ); // Create image file name

						// Check folder permission and define file location
						if( wp_mkdir_p( $upload_dir['path'] ) ) {
						    $file = $upload_dir['path'] . '/' . $filename;
						} else {
						    $file = $upload_dir['basedir'] . '/' . $filename;
						}

						// Create the image  file on the server
						file_put_contents( $file, $image_data );

						// Check image file type
						$wp_filetype = wp_check_filetype( $filename, null );

						// Set attachment data
						$attachment = array(
						    'post_mime_type' => $wp_filetype['type'],
						    'post_title'     => sanitize_file_name( $filename ),
						    'post_content'   => '',
						    'post_status'    => 'inherit'
						);

						// Create the attachment
						$attach_id = wp_insert_attachment( $attachment, $file, $talent_post_id );

						// Include image.php
						require_once(ABSPATH . 'wp-admin/includes/image.php');

						// Define attachment metadata
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

						// Assign metadata to attachment
						wp_update_attachment_metadata( $attach_id, $attach_data );

						// And finally assign featured image to post
						set_post_thumbnail( $talent_post_id, $attach_id );
						//Feature Image Code Ends
							
						
						}
						//Default Image if no image Uplaoded
						else{
							 // if($category == 'culture'){	
								// set_post_thumbnail( $talent_post_id, 4306 );	
							 // }
							 // if($category == 'actor'){	
								// set_post_thumbnail( $talent_post_id, 4307 );	
							 // }
							 // if($category == 'music'){	
								// set_post_thumbnail( $talent_post_id, 4309 );	
							 // } 
							 // if($category == 'dancer'){	
								// set_post_thumbnail( $talent_post_id, 4310 );	
							 // } 
							 // if($category == 'model'){	
								// set_post_thumbnail( $talent_post_id, 4308 );
							 // }
							
							$gender_check = '';	
							
							//Male
							if($tel_music_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_actor_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_model_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_statist_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_voiceover_gender == 'male'){	
								$gender_check = 'male';
							 }
							if($tel_gender == 'male'){	
								$gender_check = 'male';
							 }
							
							//Female
							if($tel_music_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_actor_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_model_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_statist_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_voiceover_gender == 'female'){	
								$gender_check = 'female';
							 }
							if($tel_gender == 'female'){	
								$gender_check = 'female';
							 }
							 
							 //Condition
							 if ($gender_check == 'male') {
							 	set_post_thumbnail( $talent_post_id, 4825 );
							 }
							 else if ($gender_check == 'female') {
							 	set_post_thumbnail( $talent_post_id, 4826 );
							 }							 	
						}
						//Default Image if no image Uplaoded Ends
											
						//Get Post Title 
						//$tel_title = $firstname." ".$lastname;
						
						// Update post title
						$my_post_tel = array(
						      'ID'           => $talent_post_id,
						      'post_title'   => $full_name,
							  'post_content' => $your_bio
						 );

						// Update the post into the database
						wp_update_post( $my_post_tel );					
						update_post_meta( $talent_post_id, 'city', $city );
						$user_id = get_current_user_id();
						//$website = 'http://example.com';
						$user_data = wp_update_user( array(
							'ID' => $user_id, 
							'first_name' => $firstname,
							'last_name' => $lastname,
							'user_pass' => $user_pass,
							'user_email' => $email ) );						 
						if ( is_wp_error( $user_data ) ) {
						    // There was an error; possibly this user doesn't exist.
						    //echo 'Error.';
						} else {
						   //echo 'Success!';
						   echo 'User profile updated.';
						}
						
						
						//Refrence
						$tel_company_name_cs = implode("|",$tel_company_name);
						$tel_designation_cs = implode("|",$tel_designation);
						$tel_job_profile_cs = implode("|",$tel_job_profile);
						$tel_job_duration_from_cs = implode("|",$tel_job_duration_from);
						$tel_job_duration_to_cs = implode("|",$tel_job_duration_to);
						update_post_meta( $post_ins_id, 'company_name', $tel_company_name_cs );
						update_post_meta( $post_ins_id, 'designation', $tel_designation_cs );
						update_post_meta( $post_ins_id, 'job_profile', $tel_job_profile_cs );
						update_post_meta( $post_ins_id, 'job_duration_from', $tel_job_duration_from_cs );
						update_post_meta( $post_ins_id, 'job_duration_to', $tel_job_duration_to_cs );


						//Culture
						if($category == 'culture'){
						
						$tel_culture_expertise = $_POST['culture_expertise'];
						$tel_culture_experience = $_POST['culture_experience'];
						$tel_culture_experience_description = $_POST['culture_experience_description'];
						$tel_cul_gender = $_POST['cul_gender'];

						
						update_post_meta( $talent_post_id, 'gender', $tel_cul_gender );
						
						if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
						update_post_meta( $talent_post_id, 'expertise', $tel_culture_expertise );
						update_post_meta( $talent_post_id, 'experience', $tel_culture_experience );
						update_post_meta( $talent_post_id, 'experince_description', $tel_culture_experience_description );
						update_post_meta( $talent_post_id, 'phone', $phone );
						update_post_meta( $talent_post_id, 'email', $email );
						update_post_meta( $talent_post_id, 'address', $address );
						update_post_meta( $talent_post_id, 'assignment', $assignment );
						}
						 elseif($category == 'actor'){	
						update_post_meta( $talent_post_id, 'age', $tel_actor_age );
						update_post_meta( $talent_post_id, 'gender', $tel_actor_gender );
						update_post_meta( $talent_post_id, 'height', $tel_actor_height );
						update_post_meta( $talent_post_id, 'weight', $tel_actor_weight );
						update_post_meta( $talent_post_id, 'hair_color', $tel_actor_hair_color );
						update_post_meta( $talent_post_id, 'language', $tel_actor_language );
						update_post_meta( $talent_post_id, 'experience', $tel_actor_experience );
						update_post_meta( $talent_post_id, 'phone', $phone );
						update_post_meta( $talent_post_id, 'email', $email );
						update_post_meta( $talent_post_id, 'address', $address );
						update_post_meta( $talent_post_id, 'assignment', $assignment );
						if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
						}
						elseif($category == 'music'){
						$music_category = $_POST['music_category'];
						$band_category = $_POST['band_category'];
													
						update_post_meta( $talent_post_id, 'music_gender', $tel_music_gender );

						if($music_category == 'band'){
							$music_categoies = array($music_category,$band_category);
							$tel_band_genre = $_POST['band_genre'];
							$tel_founded = $_POST['founded'];
							$tel_repertoire = $_POST['repertoire'];
							$tel_crew = $_POST['crew'];

							update_post_meta( $talent_post_id, 'band_type', $band_category );
							update_post_meta( $talent_post_id, 'genre', $tel_band_genre );
							update_post_meta( $talent_post_id, 'crew', $tel_crew );
							update_post_meta( $talent_post_id, 'repertoire', $tel_repertoire );
							update_post_meta( $talent_post_id, 'founded', $tel_founded );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							wp_set_object_terms( $talent_post_id, $music_categoies, 'music_category');

							}elseif($music_category == 'dj'){

							$dj_experience = $_POST['dj_experience'];
							$dj_description = $_POST['dj_description'];
							update_post_meta( $talent_post_id, 'experience', $dj_experience );
							update_post_meta( $talent_post_id, 'experince_description', $dj_description );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							wp_set_object_terms( $talent_post_id, $music_category, 'music_category');
								
							}elseif($music_category == 'singer'){
								$singer_experience = $_POST['singer_experience'];
								$singer_description = $_POST['singer_description'];
								update_post_meta( $talent_post_id, 'music_experience', $singer_experience );
								update_post_meta( $talent_post_id, 'music_experince_description', $singer_description );
								update_post_meta( $talent_post_id, 'phone', $phone );
								update_post_meta( $talent_post_id, 'email', $email );
								update_post_meta( $talent_post_id, 'address', $address );
								update_post_meta( $talent_post_id, 'assignment', $assignment );
								if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
								wp_set_object_terms( $talent_post_id, $music_category, 'music_category');
							}elseif($music_category == 'instrumentals'){
								$type_instrument = $_POST['type_instrument'];
								$instrumental_experience = $_POST['instrumental_experience'];
								$instrumental_description = $_POST['instrumental_description'];
								$tel_crew = $_POST['crew'];
								update_post_meta( $talent_post_id, 'type_instrument', $type_instrument );
								update_post_meta( $talent_post_id, 'instrumental_experience', $instrumental_experience );
								update_post_meta( $talent_post_id, 'music_experince_description', $instrumental_description );
								update_post_meta( $talent_post_id, 'phone', $phone );
								update_post_meta( $talent_post_id, 'email', $email );
								update_post_meta( $talent_post_id, 'address', $address );
								update_post_meta( $talent_post_id, 'assignment', $assignment );
								if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
								wp_set_object_terms( $talent_post_id, $music_category, 'music_category');
							}
							//wp_set_object_terms( $talent_post_id, $band_category, 'music_category');

						}
					
						 elseif($category == 'film'){	
							update_post_meta( $talent_post_id, 'age', $tel_age );
							update_post_meta( $talent_post_id, 'gender', $tel_gender );
							update_post_meta( $talent_post_id, 'height', $tel_height );
							update_post_meta( $talent_post_id, 'weight', $tel_weight );
							update_post_meta( $talent_post_id, 'hair_color', $tel_hair_color );
							update_post_meta( $talent_post_id, 'dialect', $tel_dialect );
							update_post_meta( $talent_post_id, 'language', $tel_language );
							update_post_meta( $talent_post_id, 'experience', $tel_experience );
							update_post_meta( $talent_post_id, 'talent_audio', $talent_audio );
							update_post_meta( $talent_post_id, 'experience_description', $tel_experienc_description );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							wp_set_object_terms( $talent_post_id, $film_category, 'film_category');
						 }
						 elseif($category == 'dancer'){
							update_post_meta( $talent_post_id, 'dance_experience', $dance_experience );
							update_post_meta( $talent_post_id, 'dance_experience_description', $dance_experience_description );
							update_post_meta( $talent_post_id, 'dancer_type', $dancer_type );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							wp_set_object_terms( $talent_post_id, $dancer_type, 'dancer_category');
						 } 
						elseif($category == 'model'){	
						 //Model
							update_post_meta( $talent_post_id, 'age', $tel_model_age );
							update_post_meta( $talent_post_id, 'gender', $tel_model_gender );
							update_post_meta( $talent_post_id, 'height', $tel_model_height );
							update_post_meta( $talent_post_id, 'mweight', $tel_model_weight );
							update_post_meta( $talent_post_id, 'exprience', $tel_model_exprience );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							update_post_meta( $talent_post_id, 'assignment', $assignment );
						}
						if($slected_category == 'sports'){
							$tel_sports_expertise = $_POST['sports_expertise'];
							$tel_sports_experience = $_POST['sports_experience'];
							$tel_sports_experience_description = $_POST['sports_experience_description'];
							$tel_cul_gender = $_POST['sports_gender'];
							$cul_sports_age = $_POST['sports_age'];
							$tel_cul_video_url = $talent_video_url;
							$tel_cul_image_url = $talent_image;
							update_post_meta( $talent_post_id, 'gender', $tel_cul_gender );
							update_post_meta( $talent_post_id, 'age', $cul_sports_age );
							if($talent_video_url != NULL) {
								update_post_meta( $talent_post_id, 'gallery_videos', $talent_video_url );
							}
							update_post_meta( $talent_post_id, 'expertise', $tel_sports_expertise );
							update_post_meta( $talent_post_id, 'experience', $tel_sports_experience );
							update_post_meta( $talent_post_id, 'experince_description', $tel_sports_experience_description );
							update_post_meta( $talent_post_id, 'phone', $phone );
							update_post_meta( $talent_post_id, 'email', $email );
							update_post_meta( $talent_post_id, 'address', $address );
							update_post_meta( $talent_post_id, 'assignment', $assignment );
							wp_set_object_terms( $talent_post_id, $slected_category, 'culture_categories');
						}
						// Refrence list
						$tel_company_name_cs = implode("|",$tel_company_name);
						$tel_designation_cs = implode("|",$tel_designation);
						$tel_job_profile_cs = implode("|",$tel_job_profile);
						$tel_job_duration_from_cs = implode("|",$tel_job_duration_from);
						$tel_job_duration_to_cs = implode("|",$tel_job_duration_to);
						update_post_meta( $talent_post_id, 'company_name', $tel_company_name_cs );
						update_post_meta( $talent_post_id, 'designation', $tel_designation_cs );
						update_post_meta( $talent_post_id, 'job_profile', $tel_job_profile_cs );
						update_post_meta( $talent_post_id, 'job_duration_from', $tel_job_duration_from_cs );
						update_post_meta( $talent_post_id, 'job_duration_to', $tel_job_duration_to_cs );

					}
				}
			}

				//Custom Edit code				
				//echo $talent_post_id;
				//$tel_email = get_post_meta( $talent_post_id, 'email', 'true' );

				$post_inner_data = get_post_meta($talent_post_id);
				
				//Get Current Post type
				$current_post_type_get = get_post($talent_post_id);
				$current_post_type = $current_post_type_get->post_type;
				$content = $current_post_type_get->post_content;
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]&gt;', $content);
				$content = strip_tags($content);
				//Title
				$post_title = $current_post_type_get->post_title;
				$data_firstname = explode(' ',trim($post_title));
				//echo $data_firstname[0]; 
				$data_lastname = $data_firstname[1].$data_firstname[2].$data_firstname[3].$data_firstname[4];

				//Film
				$data_age = get_post_meta( $talent_post_id, 'age', true );
				$data_gender = get_post_meta( $talent_post_id, 'gender', true );
				$data_height = get_post_meta( $talent_post_id, 'height', true );
				$data_weight = get_post_meta( $talent_post_id, 'weight', true );
				$data_hair_color = get_post_meta( $talent_post_id, 'hair_color', true );
				$talent_audio = get_post_meta( $talent_post_id, 'talent_audio', true  );
				$data_language = get_post_meta( $talent_post_id, 'language', true );
				$data_experience = get_post_meta( $talent_post_id, 'experience', true );
				$data_phone = get_post_meta( $talent_post_id, 'phone', true );
				$data_email = get_post_meta( $talent_post_id, 'email', true );
				$data_address = get_post_meta( $talent_post_id, 'address', true );
				$data_assignment = get_post_meta( $talent_post_id, 'assignment', true );
				$data_dialect = get_post_meta( $talent_post_id, 'dialect', true );
				
				$data_film_category_term = wp_get_post_terms($talent_post_id, 'film_category');
				$data_film_category = $data_film_category_term[0]->slug;
				
				//echo $data_film_category;

				//Dance
				$data_dancer = get_post_meta( $talent_post_id, 'dancer', true );
				$data_dance_experience = get_post_meta( $talent_post_id, 'dance_experience', true );
				$data_dance_experience_description = get_post_meta( $talent_post_id, 'dance_experience_description', true );
				$data_dancer_type = get_post_meta( $talent_post_id, 'dancer_type', true );
				$data_city = get_post_meta( $talent_post_id, 'city', true );
				//Music
				$data_type_instrument = get_post_meta( $talent_post_id, 'type_instrument', true );
				if(is_array ($data_type_instrument)){
					$data_type_instrument = $data_type_instrument[0];
				}
				$data_instrumental_experience = get_post_meta( $talent_post_id, 'instrumental_experience', true );
				if(is_array ($data_instrumental_experience)){
					$data_instrumental_experience = $data_instrumental_experience[0];
				}
				$data_crew = get_post_meta( $talent_post_id, 'crew', true );
				if(is_array ($data_crew)){
					$data_crew = $data_crew[0];
				}
				$data_singer_x_solo_x_core_experience = get_post_meta( $talent_post_id, 'singer_x_solo_x_core_experience', true );
				if(is_array ($data_singer_x_solo_x_core_experience)){
					$data_singer_x_solo_x_core_experience = $data_singer_x_solo_x_core_experience[0];
				}
				$data_band_type = get_post_meta( $talent_post_id, 'band_type', true );
				if(is_array ($data_band_type)){
					$data_band_type = $data_band_type[0];
				}
				$dj_experience = get_post_meta( $talent_post_id, 'experience' );
				if(is_array ($dj_experience)){
					$dj_experience = $dj_experience[0];
				}
				$dj_description = get_post_meta( $talent_post_id, 'experince_description');
				if(is_array ($dj_description)){
					$dj_description = $dj_description[0];
				}
				$singer_experience = get_post_meta( $talent_post_id, 'music_experience' );
				if(is_array ($singer_experience)){
					$singer_experience = $singer_experience[0];
				}
				$singer_description = get_post_meta( $talent_post_id, 'experince_description' );
				if(is_array ($singer_description)){
					$singer_description = $singer_description[0];
				}
				$type_instrument = get_post_meta( $talent_post_id, 'type_instrument' );
				if(is_array ($type_instrument)){
					$type_instrument = $type_instrument[0];
				}
				$instrumental_experience = get_post_meta( $talent_post_id, 'instrumental_experience' );
				if(is_array ($instrumental_experience)){
					$instrumental_experience = $instrumental_experience[0];
				}
				$instrumental_description = get_post_meta( $talent_post_id, 'experince_description' );
				if(is_array ($instrumental_description)){
					$instrumental_description = $instrumental_description[0];
				}
				$dance_experience_description = get_post_meta( $talent_post_id, 'experience_description' );
				if(is_array ($dance_experience_description)){
					$dance_experience_description = $dance_experience_description[0];
				}
				$tel_sports_experience_description = get_post_meta( $talent_post_id, 'experince_description', $tel_sports_experience_description );
				if(is_array ($tel_sports_experience_description)){
					$tel_sports_experience_description = $tel_sports_experience_description[0];
				}
				 /* grab the url for the full size featured image */
				$featured_img_url = get_the_post_thumbnail_url($talent_post_id,'full'); 
				
				//Sports
				$tel_sports_experience = update_post_meta( $talent_post_id, 'culture_experience', $tel_sports_experience );
				
				$tel_band_genre = get_post_meta( $talent_post_id, 'genre' );
				if(is_array ($tel_band_genre)){
					$tel_band_genre = $tel_band_genre[0];
				}
				$tel_crew = get_post_meta( $talent_post_id, 'crew' );
				if(is_array ($tel_crew)){
					$tel_crew = $tel_crew[0];
				}
				$tel_repertoire = get_post_meta( $talent_post_id, 'repertoire' );
				if(is_array ($tel_repertoire)){
					$tel_repertoire = $tel_repertoire[0];
				}
				$tel_founded = get_post_meta( $talent_post_id, 'founded' );
				if(is_array ($tel_founded)){
					$tel_founded = $tel_founded[0];
				}
				$tel_sports_expertise = get_post_meta( $talent_post_id, 'expertise');
				$tel_sports_experience = get_post_meta( $talent_post_id, 'experience' );
				$tel_sports_experience_description = get_post_meta( $talent_post_id, 'experince_description');
				$music_category = wp_get_post_terms($talent_post_id, 'music_category');
				$experience_description = get_post_meta( $talent_post_id, 'experience_description');
				$get_posted_images_url = get_post_meta( $talent_post_id, 'gallery_images', true );
				$get_posted_videos_url = get_post_meta( $talent_post_id, 'gallery_videos', true );
				$video_custom = get_post_meta( $talent_post_id, 'video_custom', true );
				
				// Get Refrence List
				$tel_company_name = get_post_meta( $talent_post_id, 'company_name', true  );
				$tel_designation = get_post_meta( $talent_post_id, 'designation', true  );
				$tel_job_profile = get_post_meta( $talent_post_id, 'job_profile', true );
				$tel_job_duration_from = get_post_meta( $talent_post_id, 'job_duration_from', true  );
				$tel_job_duration_to = get_post_meta( $talent_post_id, 'job_duration_to', true );
				$tel_company_name_arr1 = explode('|', $tel_company_name);
				$tel_designation_arr1 = explode('|', $tel_designation);
				$tel_job_profile_arr1 = explode('|', $tel_job_profile);
				$tel_job_duration_from_arr1 = explode('|', $tel_job_duration_from);
				$tel_job_duration_to_arr1 = explode('|', $tel_job_duration_to);
				$tel_company_name_arr_f = array_filter($tel_company_name_arr1, function($value) { return $value !== ''; });
				$tel_designation_arr_f = array_filter($tel_designation_arr1, function($value) { return $value !== ''; });
				$tel_job_profile_arr_f = array_filter($tel_job_profile_arr1, function($value) { return $value !== ''; });
				$tel_job_duration_from_arr_f = array_filter($tel_job_duration_from_arr1, function($value) { return $value !== ''; });
				$tel_job_duration_to_arr_f = array_filter($tel_job_duration_to_arr1, function($value) { return $value !== ''; });
				print_r($tel_company_name_arr); ?></pre><?php

				$tel_company_name_arr = array_reverse($tel_company_name_arr_f,true);
				$tel_designation_arr = array_reverse($tel_designation_arr_f,true);
				$tel_job_profile_arr = array_reverse($tel_job_profile_arr_f,true);
				$tel_job_duration_from_arr = array_reverse($tel_job_duration_from_arr_f,true);
				$tel_job_duration_to_arr = array_reverse($tel_job_duration_to_arr_f,true);

/* 				$tel_company_name_key_arr = array('company' => $tel_company_name_arr);
				$tel_designation_key_arr = array('designation' => $tel_designation_arr);
				$tel_job_profile_key_arr = array('job_profile' => $tel_job_profile_arr);
				$tel_job_duration_from_key_arr = array('job_duration_from' => $tel_job_duration_from_arr);
				$tel_job_duration_to_key_arr = array('job_duration_to' => $tel_job_duration_to_arr); */
				
				
				$tel_refrence_list_arr =array_merge($tel_company_name_key_arr,$tel_designation_key_arr); 
				

				$current_user_id = get_current_user_id();
				$user_info = get_userdata($current_user_id);
				$user_pass = $user_info->user_pass;
				//$data_address_explode = explode(' ',trim($data_address));
				//$data_city = $data_address_explode[0]; 



				// works for both array and single values

				?>


<!-- #Content -->
<div id="content_wrapper" class="span12 reg_telent_wrapper">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php
if(is_user_logged_in() && function_exists('pmpro_hasMembershipLevel') && pmpro_hasMembershipLevel())
{
	global $current_user;
	?><div class="member_level_notice"><?php
	$current_user->membership_level = pmpro_getMembershipLevelForUser($current_user->ID);
	echo 'Membership Level: ' . $current_user->membership_level->name;
	?></div><?php
	
}else{
	
	?><div class="member_level_notice"><?php
	echo "Membership Level: Free Member";
	echo "<a href='http://test.talentbasen.no/membership-account/membership-levels/'> Become a Pro Member </a>";
	?></div><?php
}
?>	
<div class="row-fluid">
			
	<div class="span12 f_col moudle">
		<div id="form_container_main">
			<h2><i class="fa fa-check"></i>My profile:</h2>
			<form class="panel-login dashboard-form telent_register_form" id="register_telent_form" action="" method="post"  >
				<div class="form-group">
				            <div class="col-md-6">
								 <label for="phone_number1">Fornavn</label>
				                <input type="text" name="firstname" value="<?php echo $data_firstname[0]; ?>" placeholder="Fornavn" required1="" disabled1>
				            </div>	

				            <div class="col-md-6">
				                 <label for="phone_number1">Etternavn</label>
								<input type="text" name="lastname" value="<?php echo $data_lastname; ?>" placeholder="Etternavn" required1="" disabled1>
				            </div>
				            <div class="col-md-6">
							 <label for="city">By/Stedsnavn</label>
				                <input type="text" name="city" value="<?php echo $data_city; ?>" placeholder="By/Stedsnavn" required1="">
				            </div>
				            <div class="col-md-6">
							 <label for="phone_number1">Adresse</label>
				                <input type="text" name="street" value="<?php echo $data_address; ?>" placeholder="Adresse" required1="">
				            </div>

				            <div class="col-md-6">
							 <label for="phone_number1">Email</label>
				                <input type="email" name="email" value="<?php echo $data_email; ?>" placeholder="Email" required1="">
				            </div>	
							<div class="col-md-6">
							 <label for="phone_number1">Telefon</label>
				                <input type="text" name="phone" value="<?php echo $data_phone; ?>" placeholder="Telefon" required>
				            </div>	
				            
				            <div class="col-md-6">
				            <div id="div_talent_category">
							 <label for="phone_number1">Kategory</label>
				                <select name="category" id="talent_category" required1="">
				                    <option value="">-Kategory -</option>
				                		                 				               
				                	<?php if($current_post_type == 'culture'){ ?>
					                    <option value="dancer">Type dans</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" selected>Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>
				                	 <?php if($current_post_type == 'music'){ ?>
					                    <option value="dancer">Dance</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music" selected>Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>
				                	<?php if($current_post_type == 'dancer'){ ?>
					                    <option value="dancer" selected>Dance</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>
				                	<?php if($current_post_type == 'sports'){ ?>
					                    <option value="dancer">Dance</option>
					                    <option value="film">Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports" selected>Sports</option>
				                	<?php } ?>
				                	<?php if($current_post_type == 'film' || $current_post_type == 'actor' || $current_post_type == 'model'){ ?>
					                    <option value="dancer">Dance</option>
					                    <option value="film" selected>Film & theater</option>
					                    <option value="music">Music</option>
					                    <option value="culture" >Culture</option>
					                    <option value="sports">Sports</option>
				                	<?php } ?>


				                </select>
							</div>
							<div id="new_category" class="text-left">
							   <input type="checkbox" id="second_cat" name="terms" required1="" value="true"><span style="">Did you have another talent ?</span>
							</div>
				            </div>
							<div class="col-md-6" id="div_talent_category_second" style="display:none;">
							 <label for="phone_number1">Another Category</label>
				           <div class="mini-box"><a target="_blank" href="<?php echo site_url().'/new-talent/?profile_id='.$talent_post_id; ?>"><input type="button" name="view_profile" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="Click here"></a></div>
							<!--<div id="div_talent_category"><label for="phone_number1">Multi Talent Category</label>
							<div class="multi_category_input"><input type="checkbox" id="dancer" name="multi_category[]" class="multi_talent" required1="" value="dancer"><span style="">Dance</span></div>
							<div class="multi_category_input"><input type="checkbox" id="film" name="multi_category[]" class="multi_talent" required1="" value="film"><span style="">Film & theater</span></div>
							<div class="multi_category_input"><input type="checkbox" id="music" name="multi_category[]" class="multi_talent" required1="" value="music"><span style="">Music</span></div>
							<div class="multi_category_input"><input type="checkbox" id="culture" name="multi_category[]" class="multi_talent" required1="" value="culture"><span style="">Culture</span></div>
							<div class="multi_category_input"><input type="checkbox" id="sports" name="multi_category[]" class="multi_talent" required1="" value="sports"><span style="">Sports</span></div>
							</div> -->
							</div>
							
				    <?php if($current_post_type == 'film' || $current_post_type == 'actor' || $current_post_type == 'model'){ ?>
				  		<div id="tel_film" class="">
				  	<?php } else{ ?>
				  		<div id="tel_film" class="hide_this">
				  	<?php } ?>
						<div class="col-md-6">
							 <label for="phone_number1">Film Category</label>
								<select name="film_category" id="film_category">
									<option value="">- Film Category -</option>
									
									<?php 
						if($data_film_category == 'statist' || $data_film_category == 'voiceover' || $current_post_type == 'actor' || $current_post_type == 'model'){
									
									if($data_film_category == 'statist'){ ?>
										<option value="statist" selected="selected">Statist</option>
									<?php } else{ ?>
								  		<option value="statist">Statist</option>
								  	<?php } ?>
								  	
								  	<?php if($data_film_category == 'voiceover'){ ?>
										<option value="voiceover" selected="selected">Voiceover</option>
									<?php } else{ ?>
								  		<option value="voiceover">Voiceover</option>
								  	<?php }
								  									  	
								  	//Else for actor and model
									if($current_post_type == 'actor'){ ?>
										<option value="actor" selected="selected">Actor</option>
									<?php } else{ ?>
								  		<option value="actor">Actor</option>
								  	<?php } ?>
									
									<?php if($current_post_type == 'model'){ ?>
										<option value="model" selected="selected">Advertising Model</option>
									<?php } else{ ?>
								  		<option value="model" >Advertising Model</option>
								  	<?php } ?>
								  	<?php }else{
										?>
										<option value="statist">Statist</option>
										<option value="voiceover">Voiceover</option>
										<option value="actor">Actor</option>
										<option value="model" >Advertising Model</option>
									<?php } ?>
									
								</select>
						</div>
						</div>  
					  
					

					<?php if($current_post_type == 'music'){
						$data_music_type = $music_category[0]->slug;
					 ?><div id="tel_music" class="">
						<?php } 
							  else{ ?>
				<div id="tel_music" class="hide_this">
						<?php 
					} ?>
					<div class="col-md-6">
						 <label for="phone_number1">Music Category</label>
				                <select name="music_category" id="music_category">
				                    <option value="">- Music Category -</option>
				                    
				                    <?php 
									
									if($data_music_type == 'band'){ ?>
					                    <option value="band" selected>Band</option>
					                    <option value="dj">Dj</option>
					                    <option value="singer">Singer</option>
					                    <option value="instrumentals">Instrumentals</option>
				                	<?php } ?>
				                	<?php if($data_music_type == 'dj'){ ?>
					                    <option value="band" >Band</option>
					                    <option value="dj" selected>Dj</option>
					                    <option value="singer">Singer</option>
					                    <option value="instrumentals">Instrumentals</option>
				                	<?php } ?>
				                	<?php if($data_music_type == 'singer'){ ?>
					                    <option value="band" >Band</option>
					                    <option value="dj" >Dj</option>
					                    <option value="singer" selected>Singer</option>
					                    <option value="instrumentals">Instrumentals</option>
				                	<?php } ?>
				                	<?php if($data_music_type == 'instrumentals'){ ?>
					                    <option value="band" >Band</option>
					                    <option value="dj" >Dj</option>
					                    <option value="singer" >Singer</option>
					                    <option value="instrumentals" selected>Instrumentals</option>
				                	<?php } else{ ?>
					                    <option value="band" >Band</option>
					                    <option value="dj" >Dj</option>
					                    <option value="singer" >Singer</option>
					                    <option value="instrumentals" >Instrumentals</option>
				                	<?php }?> 

				                </select>
				    </div>
				</div>
					
					<?php 
					if($data_music_type == 'band'){ ?>
				     <div id="tel_band" class="">
				    <?php } 
				    	  else{ ?>
					<div id="tel_band" class="hide_this">
				    <?php } ?>
				   		<div class="col-md-6">
						 <label for="phone_number1">Band</label>
				                <select name="band_category">
				                    <option value="<?php echo $data_band_type; ?>" selected><?php echo $data_band_type; ?></option>
				                    <option value="party_band">Party Band</option>
				                    <option value="jazz">Jazz</option>
				                    <option value="country">Country</option>
				                    <option value="rock">rock</option>
				                    <option value="heavy_rock">Heavy Rock</option>
				                    <option value="other">Other</option>
				                </select>
				    	</div>
						<div class="col-md-12 telent_description">
							<label for="phone_number1">Genre</label>
							<textarea name="band_genre" placeholder="Genre" required1="" ><?php echo strip_tags($tel_band_genre); ?></textarea>
						</div>
						<div class="col-md-12">
							<label for="phone_number1">Founded</label>						
							<input type="text" name="founded" value="<?php echo strip_tags($tel_founded); ?>" placeholder="Founded" />
				    	</div>
						<div class="col-md-12">
							<label for="phone_number1">Repertoire</label>						
							<textarea name="repertoire" placeholder="Repertoire" ><?php echo strip_tags($tel_repertoire); ?></textarea>
				    	</div>
						<div class="col-md-12">
							<label for="phone_number1">Crew</label>						
							<textarea name="crew" placeholder="Crew" ><?php echo strip_tags($tel_crew); ?></textarea>
				    	</div>
					</div>
					
					<?php if($data_band_type == 'dj'){ ?>
				     <div id="tel_dj_experience" class="">
				    <?php } 
				    	  else{ ?>
				     <div id="tel_dj_experience" class="hide_this">
				    <?php } ?>

						<div class="col-md-6">
							<label for="phone_number1">Dj Experience</label>			            
				                <select name="dj_experience">
				                    <?php if($dj_experience){
									?><option value="<?php echo $dj_experience; ?>" selected>- <?php echo $dj_experience; ?> -</option>	<?php
									} else { ?>
									<option value="" selected>-Select Experience-</option>	<?php
									} ?>
									
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
						</div>
						<div class="col-md-12">
							<textarea name="dj_description" placeholder="Dj Experience" ><?php echo strip_tags($dj_description); ?></textarea>
				    	</div>
					</div>
					

					<?php if($data_band_type == 'singer'){ ?>
				     <div id="tel_singer" class="">
				    <?php } 
				    	  else{ ?>
				     <div id="tel_singer" class="hide_this">
				    <?php } ?>
						<div class="col-md-6">
							<label for="phone_number1">Singer Experience</label>			            
				                <select name="singer_experience">
				                    <option value="<?php echo $singer_experience; ?>">- <?php echo $singer_experience; ?> -</option>
									 <?php if($singer_experience){
									?><option value="<?php echo $singer_experience; ?>" selected>- <?php echo $singer_experience; ?> -</option>	<?php
									} else { ?>
									<option value="" selected>-Select Experience-</option>	<?php
									} ?>
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
							</div>
						<div class="col-md-12">
								<textarea name="singer_description" placeholder="Singer Experience" ><?php echo strip_tags($singer_description); ?></textarea>
				    	</div>
					</div>
					
					<?php if($data_band_type == 'instrumentals'){ ?>
				     <div id="tel_instrumentals" class="">
				    <?php } 
				    	  else{ ?>
				     <div id="tel_instrumentals" class="hide_this">
				    <?php } ?>
						<div class="col-md-6">
							<label for="phone_number1">Type of instrument</label>						
							<input type="text" value="<?php if($data_type_instrument) print_r($data_type_instrument); ?>" name="type_instrument" placeholder="Type of instrument" />
						</div>
						<div class="col-md-6">
							<label for="phone_number1">Played number of years</label>
							<input type="text" value="<?php if($instrumental_experience) echo $instrumental_experience; ?>" name="instrumental_experience" placeholder="Enter years" />
						</div>
						<div class="col-md-12">
							<textarea name="instrumental_description" placeholder="instrumentals Experience" ><?php echo strip_tags($instrumental_description);?></textarea>
						</div>
					</div>
				</div>

				<?php if($current_post_type == 'dancer'){ ?>
			     <div id="tel_dance" class="">
			    <?php } 
			        else{ ?>
			     <div id="tel_dance" class="hide_this">
			    <?php } ?>
					<div class="col-md-6">
						<label for="phone_number1">Dance Type</label>			            
							<select name="dancer_type" id="dancer_type">
								<option value="">- Dance Type -</option>
								
								<?php if($data_dancer_type == 'hip_hop'){ ?>
									<option value="modern_dance">Modern dance</option>
									<option value="hip_hop" selected>Hip Hop</option>
									<option value="ballet">Ballet</option>
								<?php } ?>	
								<?php if($data_dancer_type == 'modern_dance'){ ?>
									<option value="modern_dance" selected>Modern dance</option>
									<option value="hip_hop" >Hip Hop</option>
									<option value="ballet">Ballet</option>
								<?php } ?>	
								<?php if($data_dancer_type == 'ballet'){ ?>
									<option value="modern_dance" >Modern dance</option>
									<option value="hip_hop" >Hip Hop</option>
									<option value="ballet" selected>Ballet</option>
								<?php }  
								 else{ ?>
									<option value="modern_dance" >Modern dance</option>
									<option value="hip_hop" >Hip Hop</option>
									<option value="ballet" >Ballet</option>
								<?php } ?>	

							</select>
					</div>
				<div class="col-md-12">
					<div id="tel_modern_dance" class="hide_this">
			            <div class="col-md-6" style="padding-left: 0;">
							<label for="phone_number1">Erfaring</label>			            
				                <select name="dance_experience">
				                    <option value="">- Erfaring -</option>
				                    
				                    <option value="low_experience">Low experience</option>
				                    <option value="medium">Medium</option>
				                    <option value="professional">Professional</option>
				                </select>
								</div>
							<div class="col-md-12" style="padding-left: 0;">
								<textarea name="dance_experience_description" placeholder="Dance Experience" ><?php echo strip_tags($dance_experience_description); ?></textarea>
							</div>
					</div>
				</div>
				</div>
				
					
						<?php if($data_film_category == 'statist'){ ?>
							<div id="tel_statist" class="">
						<?php } else{ ?>
					  		<div id="tel_statist" class="hide_this">
					  	<?php } ?>

			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="statist_age" >  
								<option value="<?php echo $data_age; ?>" selected="selected"> -- Selected Age: <?php echo $data_age; ?> -- </option>
								<option value="18">18</option><option value="19">19</option>
								<option value="20">20</option><option value="21">21</option>
								<option value="22">22</option><option value="23">23</option>
								<option value="24">24</option><option value="25">25</option>
								<option value="26">26</option><option value="27">27</option>
								<option value="28">28</option><option value="29">29</option>
								<option value="30">30</option><option value="31">31</option>
								<option value="32">32</option><option value="33">33</option>
								<option value="34">34</option><option value="35">35</option>
								<option value="36">36</option><option value="37">37</option>
								<option value="38">38</option><option value="39">39</option>
								<option value="40">40</option><option value="41">41</option>
								<option value="42">42</option><option value="43">43</option>
								<option value="44">44</option><option value="45">45</option>
								<option value="46">46</option><option value="47">47</option>
								<option value="48">48</option><option value="49">49</option>
								<option value="50">50</option><option value="51">51</option>
								<option value="52">52</option><option value="53">53</option>
								<option value="54">54</option><option value="55">55</option>
								<option value="56">56</option><option value="57">57</option>
								<option value="58">58</option><option value="59">59</option>
								<option value="60">60</option>
							</select>
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="statist_gender">
								<option value="">- Select Gender -</option>

								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
									<option value="male" >- Male -</option>
									<option value="female" selected>- Female -</option>
								<?php	} ?>
							</select>
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="statist_height" value="<?php echo $data_height; ?>" placeholder="Height" >
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="statist_weight" value="<?php echo $data_weight; ?>" placeholder="Weight" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Hair color</label>
			                <input type="text" name="statist_hair_color" value="<?php echo $data_hair_color ?>" placeholder="Hair color" >
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Dialect</label>
			                <input type="text" name="statist_dialect" value="<?php echo $data_dialect ?>" placeholder="Dialect" >
			            </div>	
				    	
			            <div class="col-md-6">
						<label for="phone_number1">Language</label>
			                <input type="text" name="statist_language" value="<?php echo $data_language; ?>" placeholder="Language" >
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="statist_experience">
								<option value="">- Select Experience -</option>
								<?php if($data_experience == 'low_experience'){ ?>
									<option value="low_experience" selected="selected">Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'medium'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium" selected="selected">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'professional'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional" selected="selected">Professional</option>
								<?php	} ?>
							</select>	
							
			            </div>	
				    </div>
				    <div id="tel_actor" class="hide_this">
			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="actor_age" >
								<option value="<?php echo $data_age; ?>" selected="selected"> -- Selected Age: <?php echo $data_age; ?> -- </option>
								<option value="18">18</option><option value="19">19</option>
								<option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="actor_gender">
								<option value="">- Select Gender -</option>
								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
									<option value="male" >- Male -</option>
									<option value="female" selected>- Female -</option>
								<?php	} ?>
							</select>
							
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="actor_height" value="<?php echo $data_height; ?>" placeholder="Height" >
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="actor_weight" value="<?php echo $data_weight; ?>" placeholder="Weight" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Hair color</label>
			                <input type="text" name="actor_hair_color" value="<?php echo $data_hair_color ?>" placeholder="Hair color" >
			            </div>
			            <div class="col-md-6">
						<label for="phone_number1">Language</label>
			                <input type="text" name="actor_language" value="<?php echo $data_language; ?>" placeholder="Language" >
			            </div>	
			    	
			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="actor_experience">
								<option value="">- Select Experience -</option>
								<?php if($data_experience == 'low_experience'){ ?>
									<option value="low_experience" selected="selected">Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'medium'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium" selected="selected">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'professional'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional" selected="selected">Professional</option>
								<?php	} ?>
							</select>	
			            </div>	
				    </div>
				    <div id="tel_model" class="hide_this">
			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="model_age" >  
								<option value="<?php echo $data_age; ?>" selected="selected"> -- Selected Age: <?php echo $data_age; ?> -- </option>
								<option value="18">18</option><option value="19">19</option>
								<option value="20">20</option><option value="21">21</option>
								<option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
			            </div>	

			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="model_gender">
								<option value="">- Select Gender -</option>
								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
									<option value="male" >- Male -</option>
									<option value="female" selected>- Female -</option>
								<?php	} ?>
							</select>
			            </div>	
			           
			            <div class="col-md-6">
						<label for="phone_number1">Height</label>
			                <input type="text" name="model_height" value="<?php echo $data_height; ?>" placeholder="Enter Height" >
			            </div>	
			            <div class="col-md-6">
						<label for="phone_number1">Weight</label>
			                <input type="text" name="model_weight" value="<?php echo $data_weight; ?>" placeholder="Enter Weight" >
			            </div>	
						 <div class="col-md-6">
							<label for="phone_number1">Model Experience</label>			            
							<select name="model_exprience">
								<option value="">- Select Experience -</option>
								<?php if($data_experience == 'low_experience'){ ?>
									<option value="low_experience" selected="selected">Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'medium'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium" selected="selected">Medium</option>
									<option value="professional">Professional</option>
								<?php	} ?>
								<?php if($data_experience == 'professional'){ ?>
									<option value="low_experience" >Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional" selected="selected">Professional</option>
								<?php	} ?>
							</select>				    	
						</div>
				    </div>
					<div id="tel_voiceover" class="hide_this">
			           <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="voiceover_gender">
								<option value="">- Select Gender -</option>
								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
									<option value="male" >- Male -</option>
									<option value="female" selected>- Female -</option>
								<?php	} ?>
							</select>
			            </div>
						 <div class="col-md-6 text-left">
							<label for="phone_number1">Voiceover Experience</label>			            
							<input type="checkbox" name="voiceover_experience[]" required1="" value="advertising"><span class="label_span">Advertising</span>				    	
							<input type="checkbox" name="voiceover_experience[]" required1="" value="informative"><span class="label_span">Informative</span>				    	
						</div>
						<div class="col-md-12">
								<textarea name="voiceover_description" placeholder="Voiceover Experience" ><?php echo strip_tags($tel_experienc_description); ?></textarea>
				    	</div>
						<div class="col-md-12 text-left">
							<label for="email">Upload your voice</label>
							<?php echo do_shortcode('[ajax-file-upload disallow_remove_button="1" unique_identifier="my_talent_audio" on_success_set_input_value="#talent_audio" allowed_extensions="mp3,wav" on_success_alert="Your audio file was successfully uploaded !" max_size=10000]'); ?>
							<div class="click_here">click here to upload !</div>
							<textarea type="text"  class="form-control hide" name="talent_audio" value="" id="talent_audio"><?php echo strip_tags($talent_audio); ?></textarea>
						</div>
				    </div>
				    

					<?php if($current_post_type == 'culture'){ ?>
					     <div id="tel_culture" class="">
					    <?php } 
					        else{ ?>
					     <div id="tel_culture" class="hide_this">
					    <?php } ?>
						<div class="col-md-12">
								<label for="phone_number1">Experties</label>
								<textarea name="culture_expertise" placeholder="Player Experties" ><?php echo strip_tags($tel_sports_expertise); ?></textarea>
				    	</div>
			           
				    	

			            <div class="col-md-6">
						<label for="phone_number1">Experience</label>
							<select name="culture_experience">
								<option value="">- Select Experience -</option>
								<?php if($data_experience == 'low_experience'){ ?>
									<option value="low_experience" selected>Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php }?>
								<?php if($data_experience == 'medium'){ ?>
									<option value="low_experience">Low experience</option>
									<option value="medium" selected>Medium</option>
									<option value="professional">Professional</option>
								<?php }?>
								<?php if($data_experience == 'professional'){ ?>
									<option value="low_experience">Low experience</option>
									<option value="medium" >Medium</option>
									<option value="professional" selected>Professional</option>
								<?php }?>
							</select>
					   </div>	

			           <div class="col-md-12">
								<textarea name="culture_experience_description" placeholder="Description" ><?php echo strip_tags($tel_sports_experience_description); ?></textarea>
				    	</div>
			            
						<div class="col-md-6">
						<label for="cul_gender">Gender</label>
							<select name="cul_gender">
								<option value="">- Select Gender -</option>
							
							<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
							<?php	} ?>
							<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
								<option value="male" >- Male -</option>
								<option value="female" selected>- Female -</option>
							<?php	} ?>

							</select>
			            </div>

				    </div>
					<div id="tel_sports" class="hide_this">
						<div class="col-md-12">
								<label for="phone_number1">Player Experties</label>
								<textarea name="sports_expertise" placeholder="Sports Experties" ><?php echo strip_tags($tel_sports_expertise); ?></textarea>
				    	</div>
			    	
			            <div class="col-md-6">
						
						<label for="phone_number1">Experience</label>
							<select name="sports_experience">
								<option value="">- Select Experience -</option>
								
								<?php if($tel_sports_experience == 'low_experience'){ ?>
									<option value="low_experience" selected>Low experience</option>
									<option value="medium">Medium</option>
									<option value="professional">Professional</option>
								<?php }?>
								<?php if($tel_sports_experience == 'medium'){ ?>
									<option value="low_experience">Low experience</option>
									<option value="medium" selected>Medium</option>
									<option value="professional">Professional</option>
								<?php }?>
								<?php if($tel_sports_experience == 'professional'){ ?>
									<option value="low_experience">Low experience</option>
									<option value="medium" >Medium</option>
									<option value="professional" selected>Professional</option>
								<?php }?>
							</select>
					   </div>		
			            <div class="col-md-12">
							<textarea name="sports_experience_description" placeholder="Description" ><?php echo strip_tags($tel_sports_experience_description); ?></textarea>
				    	</div>
			            <div class="col-md-6">
						<label for="phone_number1">Gender</label>
							<select name="sports_gender">
								<option value="">- Select Gender -</option>
								<?php if($data_gender == 'male' || $data_gender == 'Male'){ ?>	
								<option value="male" selected>- Male -</option>
								<option value="female">- Female -</option>
								<?php	} ?>
								<?php if($data_gender == 'female' || $data_gender == 'Female'){ ?>	
								<option value="male" >- Male -</option>
								<option value="female" selected>- Female -</option>
							<?php	} ?>
							</select>
			            </div>	 

			            <div class="col-md-6">
						<label for="phone_number1">Age</label>
							<select name="sports_age" >  
								<option value="<?php echo $data_age; ?>" selected="selected"> -- Selected Age: <?php echo $data_age; ?> -- </option>
								<option value="18">18</option><option value="19">19</option>
								<option value="20">20</option><option value="21">21</option>
								<option value="22">22</option><option value="23">23</option>
								<option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
							
			            </div>
				    </div>

	 <div class="col-md-6">
					<label for="phone_number1">Tar oppdrag</label>
		                <select name="assignments" required1="">
		                    <option value=""> - Tar oppdrag - </option>
		                    
		                    <?php if($data_assignment == 'Anytime'){ ?>
		                    <option value="Anytime" selected="selected"> Anytime </option>
		                    <option value="By appointment"> By appointment </option>
		                    <option value="Only via agency and agent">Only via agency and agent</option>
		                    <?php	} ?>
		                    <?php if($data_assignment == 'By appointment'){ ?>
		                    <option value="Anytime"> Anytime </option>
		                    <option value="By appointment" selected="selected"> By appointment </option>
		                    <option value="Only via agency and agent">Only via agency and agent</option>
		                    <?php	} ?>
		                    <?php if($data_assignment == 'Only via agency and agent'){ ?>
		                    <option value="Anytime"> Anytime </option>
		                    <option value="By appointment"> By appointment </option>
		                    <option value="Only via agency and agent" selected="selected">Only via agency and agent</option>
		                    <?php	} ?>

		                </select>
		            </div>

		            <div class="col-md-6">
		                <label for="phone_number1">Password</label>
						<input type="password" name="password" placeholder="Password" value="<?php echo $user_pass;  ?>" required1="" >
		            </div>
					<div class="form-group">
						<div class="col-md-12">
							<label for="phone_number1">Din profil</label>
							<textarea name="your_bio" placeholder="Write about yourself"><?php echo $content; ?></textarea>
							<div class="text-left">(Please enter comma seprated)</div>
			            </div>
					</div>					
					<div class="form-group" id="tel_gallery">
					  <div class="col-md-12">
						<label for="phone_number1">Video URLs(Youtube/Dailymotion/Vimeo etc..)</label>
			                <textarea name="video_url" placeholder="Please put Youtube video URLs e.g https://www.youtube.com/watch?v=pvXsvUOfgfo" ><?php echo strip_tags($get_posted_videos_url);?></textarea>
							<div class="text-left">(Please enter comma seprated)</div>
			            </div>
					<div class="col-md-12">
						<label for="phone_number1">Last opp CV asda</label>
						<?php echo do_shortcode('[ajax-file-upload unique_identifier="cv_upload" allowed_extensions="pdf,docx,doc,jpg,png" disallow_remove_button="1" on_success_set_input_value="#talent_cv" on_success_alert="Your CV has been successfully uploaded !"]'); ?>
						<div class="click_here">click here to upload !</div>
						<textarea class="form-control hide" name="talent_cv" value="" id="talent_cv"></textarea>
	
					</div>
					<div class="col-md-12 telent_image">
							<label for="email">Profile Image</label>
							<?php echo do_shortcode('[ajax-file-upload unique_identifier="profile_image" set_image_source="img.profile-pic" disallow_remove_button="1" on_success_set_input_value="#talent_image" allowed_extensions="jpg,png,jpeg" on_success_alert="Your images has been successfully uploaded !"]'); ?>
							<div class="click_here">click here to upload !</div>
							<textarea class="form-control hide" name="talent_image" value="" id="talent_image"><?php echo $featured_img_url; ?></textarea>
							<div class="show_upload_img">
								<img src="" class="profile-pic"/>
							</div>
					</div>
					</div>
					<div id="tel_refrence_list" class="refrence_list">
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Firmanavn</label>
						<?php 
							foreach($tel_company_name_arr as $company_name){
							?>
								<div class="col-company-name">
									<input type="text" name="company_name[]" placeholder="Firmanavn / Oppdragsgiver" value="<?php echo $company_name; ?>" required1="" >
								</div>
						<?php
							} 
						?>
						</div>
					
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Job Profile</label>
						<?php 
						
						foreach($tel_job_profile_arr as $job_profile){
									?>
									<div class="col-job_profile">
								
					                <input type="text" name="job_profile[]" placeholder="Job Profile"  value="<?php echo $job_profile; ?>" required1="" >
					            </div>
						<?php
							} 
						?>
						</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Designation</label>
						<?php 
						
							foreach($tel_designation_arr as $designation){
									?>
								<div class="col-designation">
								
					                <input type="text" name="designation[]" placeholder="Designation"  value="<?php echo $designation; ?>" required1="" >
					            </div>
						<?php
							} 
						?>
					</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Fra</label>
						<?php 
						
							foreach($tel_job_duration_from_arr as $job_duration_from){
									?>
									<div class="col-md-from">
										
						                <input type="text" name="job_duration_from[]" placeholder="Duration From"  value="<?php echo $job_duration_from; ?>" required1="" >
						            </div>
						<?php
							} 
						?>
						</div>
						<div class="refrence_list_company col-company">
						<label for="phone_number1">Til</label>
						<?php 
						
							foreach($tel_job_duration_to_arr as $job_duration_to){
									?>
									<div class="col-to">
										
						                <input type="text" name="job_duration_to[]" placeholder="Duration To"  value="<?php echo $job_duration_to; ?>" required1="" >
						            </div>
						<?php
							} 
						?>
						</div>
											
					</div>						
					<div class="form-group" id="tel_refrence_container">
						<div class="" id="tel_refrence_list">
							<div class="col-md-2">
							<label for="phone_number1">Firmanavn</label>
								<input type="text" name="company_name[]" placeholder="Firmanavn" required1="" >
							</div>
							<div class="col-md-2">
							<label for="phone_number1">Job Profile</label>
								<input type="text" name="job_profile[]" placeholder="Job Profile" required1="" >
							</div>
							<div class="col-md-2">
							<label for="phone_number1">Designation</label>
								<input type="text" name="designation[]" placeholder="Designation" required1="" >
							</div>
							<div class="col-md-2">
								<label for="phone_number1">Fra</label>
								<input class="job_duration_from" type="text" name="job_duration_from[]" placeholder="Fra" >
							</div>
							<div class="col-md-2">
								<label for="phone_number1">Til</label>
								<input class="job_duration_to" type="text" name="job_duration_to[]" placeholder="Til">
							</div>
							<div class="col-md-2" style="margin-top: 35px;">
								<div style="text-align: center;font-weight: 700;">Legg til<button id="addMore">+</button></div>
							</div>
						</div>	
					</div>	
				    <div class="final_buttons row">
					
				            <div class="col col-sm-2">
				            </div>
				            <div class="col col-sm-8">
				                <!-- <label> Upload Gallery </label> -->
				                 <div class="row">
					                <div class="col-xs-6" id="tel_upload_videos">
					                	<?php echo do_shortcode('[arfaly id="5109"]'); ?>
										<span class="upload_videos_btn">Last opp</span>
										<textarea  style="height: 0px;visibility: hidden;" title="upload_videos" type="text" class="upme-input " name="upload_videos" id="upload_videos"><?php echo $video_custom; ?></textarea>
					                </div>
					                <div class="col-xs-6" id="tel_gallery_images">
											<?php echo do_shortcode('[arfaly id="4714"]'); ?>
											<span class="upload_gallary">Last opp galleri</span>
											<textarea  style="height: 0px;visibility: hidden;" title="Gallery images" type="text" class="upme-input  upme-edit-first_name" name="gallery_images" id="gallery_images"><?php echo $get_posted_images_url; ?></textarea>	
					                </div>
					            </div>
				            </div>
				            <div class="col col-sm-2">
				            </div>
				    </div>

				    <div class="final_buttons row">
				            <div class="col col-sm-2">
				            </div>
				            <div class="col col-sm-8">
				                 <?php $get_permalink_profile = get_permalink( $talent_post_id ); 
								 $get_permalink_profile_second = get_post_meta($talent_post_id,'talent_post_id_second',true);
								 //$get_permalink_profile_second = get_permalink( $talent_post_id_second ); ?>
				                 
				                 <div class="row">
					                <div class="col-xs-6"><div class="mini-box"><input type="submit" name="register_telent" tabindex="4" class="form-control btn btn-register btn-pink" value="Lagre endringer"></div></div>
					                <div class="col-xs-6"><div class="mini-box"><a href="<?php echo $get_permalink_profile; ?>"><input type="button" name="view_profile" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="View Profile"></a></div></div>
									<?php if($get_permalink_profile_second){ ?>
									<div class="col-xs-6"><div class="mini-box"><a href="<?php echo $get_permalink_profile_second; ?>"><input type="button" name="view_profile" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="View Second Profile"></a></div></div>
									<?php } ?>
					            </div>
				            </div>
				            <div class="col col-sm-2">
				            </div>
				    </div>
				</div>
			</form>	
		</div>
	</div>
</div>	
					
					<?php
 }elseif($user_role == 'agency'){
					
				if (isset( $_POST['update_agency'] )) {
						$current_user_id = get_current_user_id();
						$user_info = get_userdata($current_user_id);
						$user_pass = $user_info->user_pass;
						$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );
						$tel_agency_name = $_POST['agency_name'];
						$tel_agency_regno = $_POST['agency_regno'];
						$tel_agency_established = $_POST['agency_established'];
						$tel_agency_staff = $_POST['agency_staff'];
						$tel_contact_person = $_POST['contact_person'];
						$tel_agency_phone = $_POST['agency_phone'];
						$tel_agency_email = $_POST['agency_email'];
						$tel_agency_country = $_POST['agency_country'];
						$tel_agency_city = $_POST['agency_city'];
						$tel_agency_address = $_POST['agency_address'];
						$tel_agency_pass = $_POST['agency_pass'];
						$about_us = $_POST['about_us'];
						// create post object with the form values
						
						

						$userdata = array(
						    'user_login' =>  $tel_agency_name,
						    'user_email' =>  $tel_agency_email,
						    'user_pass'  =>  md5($tel_agency_pass), // no plain password here!
						    'role'  =>  'agency' // no plain password here!
						); 
						 
						//$user_id = wp_insert_user( $userdata );
						$user_status = wp_insert_user($userdata);
				
						update_user_meta( $user_status, 'talent_post_id', $post_ins_id );
						echo '<div class="alert alert-success text-center"><strong>Your</strong> profile has been successfully Updated.</div>';
						
						/* $my_cptpost_args = array(
													
						'post_title'    => $tel_agency_name,
						'post_content'  => $tel_agency_address,
						'post_status'   => 'publish',
						'post_type' => 'agency'

						);

						// insert the post into the database
						$post_ins_id = wp_insert_post( $my_cptpost_args, $wp_error); */
						//}

						//Set data to Post metas	
						update_post_meta( $post_ins_id, 'agency_name', $tel_agency_name );
						update_post_meta( $post_ins_id, 'agency_reg_no', $tel_agency_regno );
						update_post_meta( $post_ins_id, 'agency_established', $tel_agency_established );
						update_post_meta( $post_ins_id, 'staff', $tel_agency_staff );
						update_post_meta( $post_ins_id, 'tel_contact_person', $tel_contact_person );
						update_post_meta( $post_ins_id, 'phone_no', $tel_agency_phone );
						update_post_meta( $post_ins_id, 'e-mail', $tel_agency_email );
						update_post_meta( $post_ins_id, 'agency_country', $tel_agency_country );
						update_post_meta( $post_ins_id, 'agency_city', $tel_agency_city );
						update_post_meta( $post_ins_id, 'address', $tel_agency_address );
						update_post_meta( $post_ins_id, 'about_us', $about_us );
						}
						
						$current_user_id = get_current_user_id();
						$user_info = get_userdata($current_user_id);
						$user_pass = $user_info->user_pass;
						$post_ins_id = get_user_meta( $current_user_id, 'talent_post_id', true );
						$profile_link = get_permalink($post_ins_id);
						$tel_agency_name = get_post_meta( $post_ins_id, 'agency_name' );
						$tel_agency_regno = get_post_meta( $post_ins_id, 'agency_reg_no');
						$tel_agency_established = get_post_meta( $post_ins_id, 'agency_established' );
						$tel_agency_staff = get_post_meta( $post_ins_id, 'staff' );
						$tel_contact_person = get_post_meta( $post_ins_id, 'tel_contact_person');
						$tel_agency_phone= get_post_meta( $post_ins_id, 'phone_no'  );
						$tel_agency_email = get_post_meta( $post_ins_id, 'e-mail' );
						$tel_agency_country = get_post_meta( $post_ins_id, 'agency_country' );
						$tel_agency_city = get_post_meta( $post_ins_id, 'agency_city');
						$tel_agency_address = get_post_meta( $post_ins_id, 'address');
						$about_us_str = get_post_meta( $post_ins_id, 'about_us');
						$about_us = strip_tags($about_us_str[0]);

			
?>
<!-- #Content -->

	<div id="content_wrapper" class="span12">
			
					<div class="row-fluid">
				 
					<div class="span12 f_col moudle agency_wrapper">
					
						<h2><i class="fa fa-check"></i>Update Profile:</h2>
					
						<form class="panel-login agency-form" id="register_telent_form" action="" method="post"  >

				        <div class="row">

				            <div class="col-md-6">
								<label>Firmanavn / Oppdragsgiver </label>
				                <input type="text" name="agency_name" placeholder="Firmanavn / Oppdragsgiver" required="" value="<?php echo $tel_agency_name[0]; ?>" disabled1>
				            </div>	
				            <div class="col-md-6">
								<label>Organisasjonsnummer</label>
				                <input type="text" name="agency_regno" placeholder="Organisasjonsnummer" value="<?php echo $tel_agency_regno[0]; ?>" required="" >
				            </div>
				            <div class="col-md-6">
								<label>Kontaktperson /Navn</label>
				                <input type="text" name="contact_person" placeholder="Kontaktperson /Navn" value="<?php echo $tel_contact_person[0]; ?>" required="">
				            </div>
				            <div class="col-md-6">
								<label>Telefon</label>
				                <input type="text" name="agency_phone" placeholder="Telefon" value="<?php echo $tel_agency_phone[0]; ?>" required="">
				            </div>
				            <div class="col-md-6">
								<label>Email</label>
				                <input type="text" name="agency_email" placeholder="Email" value="<?php echo $tel_agency_email[0]; ?>" required="">
				            </div>
				            <div class="col-md-6 hide">
								<label>Company Name </label>
				                <input type="text" name="agency_country" placeholder="Country" value="norway" class="" required="">
				            </div>
							 <div class="col-md-6">
							 	<label>Adresse</label>
				                <input type="text" name="agency_address" placeholder="Adresse" value="<?php echo $tel_agency_address[0]; ?>" required="">
				            </div>
				            <div class="col-md-6">
								<label>By/Stedsnavn</label>
				                <input type="text" name="agency_city" placeholder="By/Stedsnavn" value="<?php echo $tel_agency_city[0]; ?>" required="">
				            </div>
				            <div class="col-md-6">
								<label>Passord</label>
				                <input type="password" name="agency_pass" placeholder="Passord" value="<?php echo $user_pass; ?>" required="">
				            </div>
							<div class="col-md-12">
								<label>Om oss</label>
				                <textarea name="about_us" placeholder="Om oss" style="z-index: auto; position: relative; line-height: 22px; font-size: 14px; transition: none 0s ease 0s; background: transparent !important;"><?php echo $about_us; ?></textarea>
				            </div>

				           	
				            	
				        </div>
					<div class="form-group update_agency">
				        <div class="row">
				            <div class="col-sm-offset-3 col-sm-3">
				                 <input type="submit" name="update_agency" tabindex="4" class="form-control btn btn-register btn-pink" value="Update Profile"> 
				            </div>
							<div class="col-sm-3">
				                 <a href="<?php echo $profile_link; ?>"><input  type="button" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="View Profile"></a>
				            </div>
<?php if($get_permalink_profile_second){ ?>
							<div class="col-sm-3">
				                 <a href="<?php echo $get_permalink_profile_second; ?>"><input  type="button" tabindex="4" class="view_profile form-control btn btn-register btn-pink" value="View Second Profile "></a>
				            </div>
<?php } ?>
				        </div>
				    </div>

				</form>
				
				</div>
				</div>
			
	</div>			
<?php 
}elseif($user_role == 'vendor'){	
	get_template_part('template-vendor-edit');
}else{	

	echo 'Please login first <a href="http://test.talentbasen.no/login">Click here</a>';
	//wp_redirect( home_url() ); exit;
}

get_footer(); ?>