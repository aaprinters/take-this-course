<?php
/**
 * The single post loop Default template
 **/

if (have_posts()) {
    the_post();

    $td_mod_single = new td_module_single($post);

    ?>


    <article id="post-<?php echo $td_mod_single->post->ID;?>" class="<?php echo join(' ', get_post_class());?>" <?php echo $td_mod_single->get_item_scope();?>>
        <div class="td-post-header td-pb-padding-side">
            <?php echo td_page_generator::get_single_breadcrumbs($td_mod_single->title); ?>

            <?php echo $td_mod_single->get_category(); ?>

            <header>
                <?php echo $td_mod_single->get_title();?>


                <?php if (!empty($td_mod_single->td_post_theme_settings['td_subtitle'])) { ?>
                    <p class="td-post-sub-title"><?php echo $td_mod_single->td_post_theme_settings['td_subtitle'];?></p>
                <?php } ?>


                <div class="meta-info">

                    <?php echo $td_mod_single->get_author();?>
                    <?php echo $td_mod_single->get_date(false);?>
                    <?php echo $td_mod_single->get_views();?>
                    <?php echo $td_mod_single->get_comments();
					?>
					<div class="wishlist-td td-module-comments"> 
					<?php
						echo do_shortcode('[show_gd_mylist_btn add_icon="fa fa-user-plus" remove_icon="fa fa-user-plus" add_label="Follow" remove_label="Un Follow" item_id="'.$post_id.'"]');
						?>
					</div>
            </header>


        </div>

        <?php echo $td_mod_single->get_social_sharing_top();?>


        <div class="td-post-content td-pb-padding-side">

        <?php
        // override the default featured image by the templates (single.php and home.php/index.php - blog loop)
        if (!empty(td_global::$load_featured_img_from_template)) {
            echo $td_mod_single->get_image(td_global::$load_featured_img_from_template);
        } else {
            echo $td_mod_single->get_image('td_640x0');
        }
        ?>
		
        <?php echo $td_mod_single->get_content();
		//echo do_shortcode('[show_gd_mylist_btn]');
		  $post_id = $td_mod_single->post->ID;
		  //echo $post_id;
			echo '<div class="wpb_text_column "><div class="row">';

					echo '<div class="col-md-12" href="'.get_permalink($post_id).'"></div><div class="col-md-6 career-btn">';
					
					//echo '</div><div class="col-md-12 career-level"><p>'.$post->post_content.'</p>';
					$post_meta = get_post_meta($post_id);
					$basic_career_courses = $post_meta['basic_career_courses'];
					if($basic_career_courses){
						echo'<h5 class="course_list">Basic Courses List</h5>';
						foreach($basic_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					$intermediate_career_courses = $post_meta['intermediate_career_courses'];
					if($intermediate_career_courses){
						echo'<h5 class="course_list">Intermediate Courses List</h5>';
						foreach($intermediate_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					$advance_career_courses = $post_meta['advance_career_courses'];
					if($advance_career_courses){
						echo'<h5 class="course_list">Advance Courses List</h5>';
						foreach($advance_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					echo '</div>';
			echo '</div></div>';
		
		?>
        
		</div>


        <footer>
            <?php echo $td_mod_single->get_post_pagination();?>
            <?php echo $td_mod_single->get_review();?>

            <div class="td-post-source-tags td-pb-padding-side">
                <?php echo $td_mod_single->get_source_and_via();?>
                <?php echo $td_mod_single->get_the_tags();?>
            </div>

            <?php echo $td_mod_single->get_social_sharing_bottom();?>
            <?php echo $td_mod_single->get_next_prev_posts();?>
            <?php echo $td_mod_single->get_author_box();?>
	        <?php echo $td_mod_single->get_item_scope_meta();?>
        </footer>

    </article> <!-- /.post -->

    <?php echo $td_mod_single->related_posts();?>

<?php
} else {
    //no posts
    echo td_page_generator::no_posts();
}